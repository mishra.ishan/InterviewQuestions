package Random;

public class IntegerBreak {

    private int[] table;

    public IntegerBreak() {
        this.table = new int[58];
        table[0] = 0;
        table[1] = 1;
        table[2] = 2;
        table[3] = 3;
        table[4] = 4;
        table[5] = 6;
        table[6] = 9;
        table[7] = 10;
        table[8] = 16;
        table[9] = 27;
        table[10] = 36;
    }

    public int integerBreak(int n) {
        if(n == 1) {
            return 0;
        }
        if(n < 4) {
            return n-1;
        }
        int maxProduct = 1;
        for(int i=2, j=n-2; i<=j; i++, j--) {
            int asum = 0, bsum = 0;
            if(table[i] > 0) {
                asum = table[i];
            }
            else {
                table[i] = integerBreak(i);
                asum = table[i];
            }
            if(table[j] > 0) {
                bsum = table[j];
            }
            else {
                table[j] = integerBreak(j);
                bsum = table[j];
            }
            if(asum*bsum > maxProduct) {
                maxProduct = asum*bsum;
            }
        }
        return maxProduct;
    }
}
