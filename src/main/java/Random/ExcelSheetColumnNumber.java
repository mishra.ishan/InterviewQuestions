package Random;

import java.lang.reflect.Field;

/**
 * Created by ishan.mishra on 9/1/17.
 */
public class ExcelSheetColumnNumber {
    public static void main(String... args) {
        String s = "ABBD";
        System.out.println(new ExcelSheetColumnNumber().titleToNumber(s));
    }
    public int titleToNumber(String s) {
        int result = 0;
        char[] chars = s.toCharArray();
        for(int i=0; i<chars.length; i++) {
            result = 26*result + (chars[i]-'A') + 1;
        }
        return result;
    }
}
