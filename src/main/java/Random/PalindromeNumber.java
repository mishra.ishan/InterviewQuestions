package Random;

//https://leetcode.com/problems/palindrome-number/description/
public class PalindromeNumber {
    public static void main(String[] args) {
        PalindromeNumber number = new PalindromeNumber();
        System.out.println(number.isPalindrom(100));
    }

    //Note that for answer to be accepted, all negative numbers are treated as non palidromic
    public boolean isPalindrom(int x) {
        int digit = 0;
        Long no = Long.parseLong(String.valueOf(x));
        no = Math.abs(no);
        for(long i = 10; i <= no; i *= 10) {
            digit++;
        }
        if(digit == 0) {
            return true;
        }
        while(digit > 0 && no / (long)Math.pow(10, digit) == no % 10) {
            long temp = (long) Math.pow(10,digit);
            no = (no - (no/temp)*temp) / 10;
            digit-=2;
        }
        if(digit <= 0) {
            return true;
        }
        return false;
    }
}
/*
191. /10: 2 digit, /100: 3 digit, /1000: 3 digit
191 / 100 = 1
191 % 10 = 1
19891 = 5 digit
19891 / 10000 = 1
19891 % 10 = 1
(19891 - (19891/10000)*10000) = 9891 / 10 = 989
989 / 100 == 989 % 10
no = (no - (no/100)*Math.exp(10,digit)) / 10
 */