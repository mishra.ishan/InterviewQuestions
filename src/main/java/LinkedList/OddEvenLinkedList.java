package LinkedList;

/**
 * Created by ishan.mishra on 8/30/17.
 */
public class OddEvenLinkedList {
    public static void main(String... args) {
        ListNode n = new ListNode(1);
//        n.appendToEnd(2);
//        n.appendToEnd(3);
//        n.appendToEnd(4);
//        n.appendToEnd(5);
        System.out.println(n);
        System.out.println(new OddEvenLinkedList().oddEvenList(n));
    }

    public ListNode oddEvenList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode secondListHead = head.next;
        ListNode firstListRunner = head, secondListRunner = secondListHead;
        while (firstListRunner != null && secondListRunner != null &&
                firstListRunner.next != null && secondListRunner.next != null) {
            firstListRunner.next = firstListRunner.next.next;
            firstListRunner = firstListRunner.next;
            secondListRunner.next = secondListRunner.next.next;
            secondListRunner = secondListRunner.next;
        }
        firstListRunner.next = secondListHead;
        return head;
    }
}
