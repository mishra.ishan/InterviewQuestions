package LinkedList;

/**
 * Created by ishan.mishra on 8/29/17.
 */

public class AddTwoNumbersII {
    public static void main(String... args) {
        ListNode l1 = new ListNode(9);
        l1.appendToEnd(9);
        l1.appendToEnd(3);
        l1.appendToEnd(8);
        System.out.println("l1: " + l1);
        ListNode l2 = new ListNode(9);
        l2.appendToEnd(9);
//        l2.appendToEnd(6);
//        l2.appendToEnd(4);
        System.out.println("l2: " + l2);
//        System.out.println("Reversed List: " + new AddTwoNumbersII().addTwoNumbers(l1, l2));
        System.out.println(new AddTwoNumbersII().addRecursive(l1, l2));
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ReverseList reverseList = new ReverseList();
        ListNode num1 = reverseList.reverse(l1);
        ListNode num2 = reverseList.reverse(l2);
//        System.out.println("num1: " + num1);
//        System.out.println("num2: " + num2);
        ListNode runner1 = num1, runner2 = num2;
        ListNode ans = null, ansRunner = null;
        byte carry = 0;
        while(runner1 != null || runner2 != null || carry == 1) {
            int r1 = runner1 != null ? runner1.val : 0;
            int r2 = runner2 != null ? runner2.val : 0;
            int val = r1 + r2 + carry;
            if(val > 9) {
                carry = 1;
            }
            else {
                carry = 0;
            }
            if(ans == null) {
                ans = new ListNode(val%10);
                ansRunner = ans;
            }
            else {
                ansRunner.next = new ListNode(val%10);
                ansRunner = ansRunner.next;
            }
//            System.out.println("After iteration, ans: " + ans);
            if(runner1 != null) {
                runner1 = runner1.next;
            }
            if (runner2 != null) {
                runner2 = runner2.next;
            }
        }
        return reverseList.reverse(ans);
    }

    public ListNode addRecursive(ListNode n1, ListNode n2) {
        int len1 = n1.count;
        int len2 = n2.count;
        while (len2 < len1) {
            n2.appendAtBeginning(0);
            len2++;
        }
        while(len2 > len1) {
            n1.appendAtBeginning(0);
            len1++;
        }
        return addRecuriveHelper(n1,n2);
    }

    private ListNode addRecuriveHelper(ListNode n1, ListNode n2) {
        if(n1 == null) {
            return null;
        }
        ListNode head = new ListNode(-1);
        head.next = addRecuriveHelper(n1.next, n2.next);
        int val = n1.val + n2.val;
        if(head.next != null && head.next.val > 9) {
            val += 1;
            head.next.val = head.next.val%10;
        }
        head.val = (val);
        return head;
    }
}
