package LinkedList;

/**
 * Created by ishan.mishra on 9/1/17.
 */
public class PalindromicList {
    ListNode left = null;
    public static void main(String... args) {
        ListNode node = new ListNode(1);
        node.appendToEnd(2);
        node.appendToEnd(4);
        node.appendToEnd(3);
        node.appendToEnd(2);
        node.appendToEnd(1);
        System.out.println(new PalindromicList().isPalindrome(node));
//        node.appendToEnd(2);
    }
    public Boolean isPalindrome (ListNode head) {
        if(head == null) {
            return true;
        }
        left = head;
        return isPalindromeHelper(head.next);
    }
    private Boolean isPalindromeHelper (ListNode right) {
        if(right == null) {
            return true;
        }
        boolean ans = isPalindromeHelper(right.next);
        if(!ans) {
            return false;
        }
        boolean ret = left.val == right.val;
        left = left.next;
        return ret;
    }
}
