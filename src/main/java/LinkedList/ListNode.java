package LinkedList;

/**
 * Created by ishan.mishra on 8/27/17.
 */
public class ListNode {
    int val;
    ListNode next;
    int count = 0;
    public ListNode(int data) {
        this.val = data;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getVal() {
        return val;
    }

    public ListNode getNext() {
        return next;
    }

    public int getCount() {
        return count;
    }

    public void appendToEnd(int val) {
        ListNode temp = this;
        while(temp.next != null) {
            temp = temp.next;
        }
        temp.next = new ListNode(val);
        ++count;
    }
    public void appendAtBeginning(int val) {
        ListNode n = new ListNode(this.val);
        this.val = val;
        ListNode temp = this.next;
        this.next = n;
        n.next = temp;
    }
    public ListNode getMiddle() {
        if(this == null || this.next == null) {
            return this;
        }
        ListNode slow = this, fast = this;
        while(fast != null && fast.next != null && fast.next.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }

    @Override
    public String toString() {
        StringBuffer ans = new StringBuffer();
        if(this == null) {
            ans.append("/");
            return ans.toString();
        }
        ListNode temp = this;
        while (temp != null) {
            ans.append(temp.val + "->");
            temp = temp.next;
        }
        ans.append("/");
        return ans.toString();
    }
}
