package LinkedList;

/**
 * Created by ishan.mishra on 8/29/17.
 */
public class ReverseList {
    public static void main(String... args) {
        ReverseList reverseList = new ReverseList();
        ListNode node = new ListNode(1);
        node.appendToEnd(2);
        node.appendToEnd(3);
        node.appendToEnd(4);
        node.appendToEnd(5);
        System.out.println(node);
        System.out.println(reverseList.reverse(node));
    }

    public ListNode reverse (ListNode head) {
        if(head == null) {
            return null;
        }
        return reverseTemp(null,head,head.next);
    }

    private ListNode reverseTemp(ListNode prev, ListNode curr, ListNode next) {
        if(next == null) {
            curr.next = prev;
            return curr;
        }
        curr.next = prev;
        return reverseTemp(curr,next,next.next);
    }
}
