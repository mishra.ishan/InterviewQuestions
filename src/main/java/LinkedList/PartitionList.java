package LinkedList;

public class PartitionList {

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.appendToEnd(2);
        head.appendToEnd(4);
        head.appendToEnd(3);
        head.appendToEnd(5);
        head.appendToEnd(2);
        System.out.println("Original List: "+head);
        System.out.println("After Partitioning list: "+new PartitionList().partition(head, 3));
    }

    public ListNode partition(ListNode head, int x) {
        if(head == null || head.next == null) {
            return head;
        }
        ListNode j = head, i = null;
        if(j.val < x) {
            //if the starting head node is already at the right place. It is done in order to reduce the corner cases in putJAfterI method.
            i = head;
        }
        while(j != null && j.next != null) {
            if(j.next.val >= x) {
                j = j.next;
            }
            else if(i == j) {
                i = i.next;
                j = j.next;
            }
            else {
                //swap the places
                head = putJAfterI(head,i,j);
                if(i == null) {
                    i = head;
                }
                else {
                    i = i.next;
                }
            }
        }
        return head;
    }

    private ListNode putJAfterI(ListNode head, ListNode i, ListNode j) {
        //for case when i is still at the beginning
        if(i == null) {
            ListNode temp1 = j.next;
            j.next = temp1.next;
            temp1.next = head;
            head = temp1;
        }
        else {
            /*
            .........i->i.next->...........j->j.next->(null or node)
            ............temp1............................temp2
             */
            ListNode temp1 = i.next, temp2 = j.next.next;
            i.next = j.next;
            j.next = temp2;
            i.next.next = temp1;
        }
        return head;
    }
}
