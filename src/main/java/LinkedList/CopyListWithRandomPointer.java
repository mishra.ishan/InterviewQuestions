package LinkedList;

class RandomListNode {
    int label;
    RandomListNode next, random;
    RandomListNode(int x) { this.label = x; }
}

//https://leetcode.com/problems/copy-list-with-random-pointer/description/
public class CopyListWithRandomPointer {

    public RandomListNode copyRandomList(RandomListNode head) {
        if(head == null) {
            return null;
        }
        createAuxList(head);
        assignRandomPointersToAuxList(head);
        return newListFromAux(head);
    }

    private RandomListNode newListFromAux(RandomListNode head) {
        RandomListNode tempMain = head, tempAux = head.next, headAux = head.next;
        while(tempMain.next.next != null) {
            tempMain.next = tempMain.next.next;
            tempAux.next = tempAux.next.next;
            tempMain = tempMain.next;
            tempAux = tempAux.next;
        }
        tempMain.next = null;
        return headAux;
    }

    private void assignRandomPointersToAuxList(RandomListNode head) {
        RandomListNode temp = head;
        while(temp != null) {
            if(temp.random != null) {
                temp.next.random = temp.random.next;
            }
            temp = temp.next.next;
        }
    }

    private void createAuxList(RandomListNode head) {
        RandomListNode itrPtr = head;
        while(itrPtr != null) {
            RandomListNode temp = new RandomListNode(itrPtr.label);
            temp.next = itrPtr.next;
            itrPtr.next = temp;
            itrPtr = temp.next;
        }
    }
}
