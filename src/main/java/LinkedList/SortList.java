package LinkedList;

/**
 * Created by ishan.mishra on 8/29/17.
 */
public class SortList {
    public static void main(String[] args) {
        ListNode node = new ListNode(1);
        node.appendToEnd(2);
//        node.appendToEnd(1);
        node.appendToEnd(4);
        System.out.println(node);
//        System.out.println(node.getMiddle());
//        ListNode left = node;
//        ListNode middle = node.getMiddle();
//        ListNode right = middle.next;
//        System.out.println("Middle: " + middle);
//        middle.next = null;
//        System.out.println("Left: " + left);
//        System.out.println("Right: " + right);
//
        System.out.println(new SortList().sortList(node));
    }
    public ListNode sortList(ListNode head) {
//        System.out.println("List received: " + head);
        if(head == null || head.next == null) {
            return head;
        }
        /*
        1. get middle
        2. mergesort first half
        3. mergesort second half
        4. merge the two halves
         */
        ListNode left = head;
        ListNode middle = head.getMiddle();
        ListNode right = middle.next;
        middle.next = null;
//        System.out.println("Left: " + left);
//        System.out.println("Right: " + right);
        left = sortList(left);
        right = sortList(right);
        return mergeTwoLinkedLists(left,right);
    }

    private ListNode mergeTwoLinkedLists(ListNode left, ListNode right) {
//        System.out.println("< --------------------- Merging request for: "+left+" and "+right);
        if(left == null) {
            return right;
        }
        if(right == null) {
            return left;
        }
        ListNode leftRunner = left, rightRunner = right, head = null, headRunner = null;
        while(leftRunner != null && rightRunner != null) {
//            System.out.println("leftRunner: "+leftRunner);
//            System.out.println("rightRunner: "+rightRunner);
            int leftVal = leftRunner.val;
            int rightVal = rightRunner.val;
            if(leftVal < rightVal) {
                if(head == null) {
                    head = leftRunner;
                    headRunner = head;
                }
                else {
                    headRunner.next = leftRunner;
                    headRunner = headRunner.next;
                }
                leftRunner = leftRunner.next;
            }
            else {
                if(head == null) {
                    head = rightRunner;
                    headRunner = head;
                }
                else {
                    headRunner.next = rightRunner;
                    headRunner = headRunner.next;
                }
                rightRunner = rightRunner.next;
            }
        }
        if(rightRunner == null) {
            headRunner.next = leftRunner;
        }
        else if(leftRunner == null) {
            headRunner.next = rightRunner;
        }
//        System.out.println("--------------------- > After Merging: "+head);
        return head;
    }
}
