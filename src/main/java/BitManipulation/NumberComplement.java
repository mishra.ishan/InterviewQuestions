package BitManipulation;

//https://leetcode.com/problems/number-complement/description/
public class NumberComplement {
    public static void main (String[] args) {
        NumberComplement numberComplement = new NumberComplement();
        System.out.println(numberComplement.findComplement(1));
    }

    public int findComplement(int num) {
        String int_32bit = Integer.toBinaryString(num);
        for(int i = 0; i < int_32bit.length(); i++) {
            int bit = getBit(num, i);
//            System.out.println("bit at "+ i + " is " + bit);
            if(bit == 1) {
                bit = 0;
            }
            else {
                bit = 1;
            }
            num = updateBit(num, i, bit);
//            System.out.println("After updating, the 32 bit integer: " + Integer.toBinaryString(num));
        }
        return num;
    }

    private int updateBit(int num, int i, int bit) {
        return (num & ~(1 << i)) | (bit << i);
    }

    private int getBit(int num, int i) {
        return (num & 1 << i) == 0 ? 0 : 1;
    }
}
