package BitManipulation;

import java.util.Scanner;

//https://practice.geeksforgeeks.org/problems/bit-difference/0
public class BitDifference {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt();
        BitDifference bitDifference = new BitDifference();
        for(int i = 0; i < t; i++) {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            System.out.println(bitDifference.noOfBits(a, b));
        }
    }

    private int noOfBits (int a, int b) {
        int xor = a ^ b;
        int ans = 0;
        for(int i = 0; i < 32; i++) {
            if((xor & (1<<i)) != 0) {
                ans++;
            }
        }
        return ans;
    }
}
