package BitManipulation;

public class Q1CrackingCodingInterview {
    public static void main (String[] args) {
        Q1CrackingCodingInterview q1 = new Q1CrackingCodingInterview();
        int m = Integer.parseInt("10011", 2);
        int n = Integer.parseInt("10000000000", 2);
        int i = 2, j = 6;
        System.out.println(q1.insertMIntoN(m,n,2,6));
    }

    private int insertMIntoN(int m, int n, int bit_low, int bit_high) {
        for(int i = bit_low; i < bit_high; i++) {
            int ithBit_m = getBit(m, i);
            n = updateBit(n, ithBit_m, i);
        }
        return n;
    }

    private int getBit (int num, int i) {
        return (num & (1 << i)) == 0 ? 0 : 1;
    }

    private int updateBit (int num, int bit, int i) {
        return ((num & ~(1 << i)) | (bit << i));
    }
}
