package RecursionAndDp;

//https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/description/
public class BestTimeToBuyAndSellStockIII {
    public static void main(String[] args) {

    }

    public int maxProfitDP(int[] p) {
        if(p == null) {
            return 0;
        }
        int n = p.length;
        if(n == 0 || n == 1) {
            return 0;
        }
        int[][][] ans = new int[n+1][3][2];
        ans[0][1][0] = 0;
        ans[0][1][1] = Integer.MIN_VALUE;
        ans[0][2][0] = 0;
        ans[0][2][1] = Integer.MIN_VALUE;
        for(int i = 1; i<= n; i++) {
            ans[i][2][0] = Math.max(ans[i-1][2][0], ans[i-1][2][1] + p[i-1]);
            ans[i][2][1] = Math.max(ans[i-1][2][1], ans[i-1][1][0] - p[i-1]);
            ans[i][1][0] = Math.max(ans[i-1][1][0], ans[i-1][1][1] + p[i-1]);
            ans[i][1][1] = Math.max(ans[i-1][1][1], -p[i-1]);
        }
        return (int) ans[n][2][0];
    }
}

/*
Since it is an opt max problem, let us define a variable array which takes input n and gives the max profit possible from days 0 to days n-1. Now, we need to think about what all can happen on the nth day? We can either buy a new stock (if we don't have any stock right now), sell an existing stock, or hold on to our current stock. Now, we also need to keep in mind that we can't perform more than two transactions. So, let us say our array is a 3D array ans[i][j][k] which gives us the max profit possible from days 0 to i, with at most j transactions possible and with k denoting the number of stocks that we have at the end of day i. So, we need to find out ans[n][2][0].

If we do nothing on the nth day, then we will have our no of transactions intact and also if we had 0 stocks, or 1 stock, we would still have the same number of stocks. Also, since transaction consists of a buy and a sale, our number of transactions will reduce either on a buy or on a sale, and not during both. Let us say our number of transactions left reduce by 1 when we have a sale, then:-

1. We have 2 transactions left
    1.1 We buy on the nth day
    1.2 We sell on the nth day
2. We have 1 transaction left
    2.1 We buy on the nth day
    2.2 We sell on the nth day.

 */