package RecursionAndDp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class RatInAMaze {
    public static void main(String[] args) throws IOException{
        RatInAMaze ratInAMaze = new RatInAMaze();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(bufferedReader.readLine());
        while(t-- > 0) {
            int n = Integer.parseInt(bufferedReader.readLine());
            int[][] m = new int[n][n];
            String s = bufferedReader.readLine();
            String[] split = s.split(" ");
            int indx = 0;
            for(int i=0; i<n; i++) {
                for(int j=0; j<n; j++) {
                    m[i][j] = Integer.parseInt(split[indx++]);
                }
            }
            System.out.println("Final ans: " + ratInAMaze.printPath(m, n));
        }
    }

    public ArrayList<String> printPath(int[][] m, int n) {
        ArrayList<String> ans = new ArrayList<>();
        boolean[][] isVisited = new boolean[n][n];
        for(int i=0; i<n; i++) {
            for(int j=0; j<n; j++) {
                isVisited[i][j] = false;
            }
        }
        backTrackSolution(m,0,0,4,"", ans, isVisited);
        return ans;
    }

    private void backTrackSolution(int[][] m, int i, int j, int n, String strTillNow, ArrayList<String> finalAns, boolean[][] isVisited) {
        if(i == n-1 && j == n-1) {
            finalAns.add(strTillNow);
            return;
        }
        if(isValid(m, i+1, j, n, isVisited)) {
            isVisited[i+1][j] = true;
            backTrackSolution(m, i+1, j, n, strTillNow + "D", finalAns, isVisited);
            isVisited[i+1][j] = false;
        }
        if(isValid(m, i-1, j, n, isVisited)) {
            isVisited[i-1][j] = true;
            backTrackSolution(m, i-1, j, n, strTillNow + "U", finalAns, isVisited);
            isVisited[i-1][j] = false;
        }
        if(isValid(m, i, j+1, n, isVisited)) {
            isVisited[i][j+1] = true;
            backTrackSolution(m, i, j+1, n, strTillNow + "R", finalAns, isVisited);
            isVisited[i][j+1] = false;
        }
        if(isValid(m, i, j-1, n, isVisited)) {
            isVisited[i][j-1] = true;
            backTrackSolution(m, i, j-1, n, strTillNow + "L", finalAns, isVisited);
            isVisited[i][j-1] = false;
        }
    }

    private boolean isValid(int[][] m, int i, int j, int n, boolean[][] isVisited) {
        return i >= 0 && i < n && j >= 0 && j<n && m[i][j] == 1 && isVisited[i][j] == false;
    }

    /*
    class GfG{
        public static ArrayList<String> printPath(int[][] m, int n)
        {
            GFG gfg = new GFG();
            ArrayList<String> ans = new ArrayList<>();
            backTrackSolution(m,0,0,n,"",ans);
            return ans;
        }

        private void backTrackSolution(int[][] m, int i, int j, int n, String strTillNow, ArrayList<String> finalAns) {
            if(i == n-1 && j == n-1) {
                finalAns.add(strTillNow);
                return;
            }
            if(isValid(m, i+1, j, n)) {
                backTrackSolution(m, i+1, j, n, strTillNow + "D", finalAns);
            }
            if(isValid(m, i-1, j, n)) {
                backTrackSolution(m, i+1, j, n, strTillNow + "U", finalAns);
            }
            if(isValid(m, i, j+1, n)) {
                backTrackSolution(m, i+1, j, n, strTillNow + "R", finalAns);
            }
            if(isValid(m, i, j-1, n)) {
                backTrackSolution(m, i+1, j, n, strTillNow + "L", finalAns);
            }
        }

        private boolean isValid(int[][] m, int i, int j, int n) {
            return i >= 0 && i < n && j >= 0 && j<n && m[i][j] == 1;
        }
    }
     */
}
