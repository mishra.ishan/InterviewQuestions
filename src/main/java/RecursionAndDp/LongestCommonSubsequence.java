package RecursionAndDp;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LongestCommonSubsequence {

    private Integer[][] table;

    public LongestCommonSubsequence(int m, int n) {
        this.table = new Integer[m][n];
    }

    public static void main(String[] args) {
        String a = "AGGTAB", b = "GXTXAYB";
        int m = a.length(), n = b.length();
        LongestCommonSubsequence longestCommonSubsequence = new LongestCommonSubsequence(m,n);
        System.out.println("Longest Common Subsequence: "+longestCommonSubsequence.longestCommonSubsequence(a, b));
        System.out.println("LCS Bottom up " + longestCommonSubsequence.bottomUpLCS(a,b));
    }

    public int longestCommonSubsequence(String a, String b) {
        return lcsHelper(a, 0, a.length(), b, 0, b.length());
    }

    private int lcsHelper(String a, int i, int n1, String b, int j, int n2) {
        if(i == n1 || j == n2) {
            return  0;
        }
        if(table[i][j] != null) {
            return table[i][j];
        }
        if(a.charAt(i) == b.charAt(j)) {
            table[i][j] =  1 + lcsHelper(a, i+1, n1, b, j+1, n2);
        }
        else {
            table[i][j] = Math.max(lcsHelper(a, i+1, n1, b, j+1, n2),
                    Math.max(lcsHelper(a, i+1, n1, b, j, n2), lcsHelper(a, i, n1, b, j+1, n2)));
        }
        return table[i][j];
    }

    private int bottomUpLCS (String a, String b) {
        int n1 = a.length(), n2 = b.length();
        int dp[][] = new int[n1+1][n2+1];
        for(int i = 0; i<n1+1; i++) {
            dp[i][0] = 0;
        }
        for(int j=0; j<n2+1; j++) {
            dp[0][j] = 0;
        }
        for(int i=1; i<=n1; i++) {
            for(int j=1; j<=n2; j++) {
                if(a.charAt(i-1) == b.charAt(j-1)) {
                    dp[i][j] = 1 + dp[i-1][j-1];
                }
                else {
                    dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]);
                }
            }
        }
        return dp[n1][n2];
    }

    private void printLCS(int[] a, int[] b) {
        int n1 = a.length, n2 = b.length;
        int[] dp[] = new int[n1+1][n2+1];
        for(int i=0; i<=n1; i++) {
            dp[i][0] = 0;
        }
        for(int j=0; j<=n2; j++) {
            dp[0][j] = 0;
        }
        for(int i=1; i<=n1; i++) {
            for(int j=1; j<=n2; j++) {
                if(a[i-1] == b[j-1]) {
                    dp[i][j] = 1 + dp[i-1][j-1];
                }
                else {
                    dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]);
                }
            }
        }
        int i = n1, j = n2;
        //System.out.println("DP TABLE:-");
        //for(int i1=0; i1<=n1; i1++) {
        //   for(int j1=0; j1<=n2; j1++) {
        //     System.out.print(dp[i1][j1]+" ");
        //}
        //System.out.println();
        //}
        //System.out.println("Printing LCS:-");
        List<Integer> ans = new ArrayList<>();
        while(i > 0 && j>0) {
            if(dp[i-1][j] == dp[i][j]) {
                while(i>0 && dp[i-1][j] == dp[i][j]) {
                    //System.out.println("GOING UP");
                    i--;
                }
                //System.out.println("UP LEFT");
                if(i>0) {
                    ans.add(a[i-1]);
                }
                i--;
                j--;
            }
            else  {
                while(j>0 && dp[i][j] == dp[i][j-1]) {
                    //System.out.println("GOING LEFT");
                    j--;
                }
                //System.out.println("LEFT UP");
                if(j>0) {
                    ans.add(b[j-1]);
                }
                i--;
                j--;
            }
        }
        for(int i1 = ans.size()-1; i1>=0; i1--) {
            System.out.print(ans.get(i1)+" ");
        }
    }

}
