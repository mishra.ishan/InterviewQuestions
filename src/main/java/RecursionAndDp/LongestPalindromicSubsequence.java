package RecursionAndDp;

//https://leetcode.com/problems/longest-palindromic-subsequence/description/
public class LongestPalindromicSubsequence {
    public static void main(String[] args) {
        String s = "";
        LongestPalindromicSubsequence lps = new LongestPalindromicSubsequence();
        System.out.println(lps.longestPalindromeSubseq(s));
        System.out.println(lps.longestPalindromicSubsequcneDP(s));
    }

    public int longestPalindromeSubseq(String s) {
        if(s == null || s.length() == 0) {
            return 0;
        }
        return longestPalindromicSubsequenceHelper(s, 0, s.length()-1);
    }

    private int longestPalindromicSubsequenceHelper (String s, int i, int j) {
        if(i > j) {
            return 0;
        }
        if(i == j) {
            return 1;
        }
        if(s.charAt(i) == s.charAt(j)) {
            return 2 + longestPalindromicSubsequenceHelper(s, i+1, j-1);
        }
        return Math.max(longestPalindromicSubsequenceHelper(s, i+1, j), longestPalindromicSubsequenceHelper(s, i, j-1));
    }

    private int longestPalindromicSubsequcneDP(String a) {
        if(a == null || a.length() == 0) {
            return 0;
        }
        String b = reverse(a);
        return longestCommonSubsequenceDP(a, b);
    }

    private int longestCommonSubsequenceDP(String a, String b) {
        int n = a.length();
        int[][] ans = new int[n+1][n+1];
        for(int i = 0; i<= n; i++) {
            for(int j = 0; j<= n; j++) {
                if(i == 0 || j == 0) {
                    ans[i][j] = 0;
                }
                else {
                    if(a.charAt(i-1) == b.charAt(j-1)) {
                        ans[i][j] = ans[i-1][j-1] + 1;
                    }
                    else {
                        ans[i][j] = Math.max(ans[i-1][j], ans[i][j-1]);
                    }
                }
            }
        }
        return ans[n][n];
    }

    private String reverse (String a) {
        StringBuffer sb = new StringBuffer();
        for(int i = a.length()-1; i >=0; i--) {
            sb.append(a.charAt(i));
        }
        return sb.toString();
    }
}
