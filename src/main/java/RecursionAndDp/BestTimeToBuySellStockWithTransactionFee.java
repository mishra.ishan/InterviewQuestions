package RecursionAndDp;

//https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/description/
public class BestTimeToBuySellStockWithTransactionFee {
    public static void main(String[] args) {
        int fee = 10;
        int[] p = {1, 2, 7, 5, 9};
        BestTimeToBuySellStockWithTransactionFee bestTimeToBuySellStockWithTransactionFee = new BestTimeToBuySellStockWithTransactionFee();
        System.out.println(bestTimeToBuySellStockWithTransactionFee.maxProfitPossibleDP(p, fee));
    }

    public int maxProfitPossibleDP(int[] p, int fee) {
        if(p == null) {
            return 0;
        }
        int n = p.length;
        if(n == 0 || n == 1) {
            return 0;
        }
        int max = 0;
        for(int i = 0; i<n; i++) {
            if(p[i] > max) {
                max = p[i];
            }
        }
        int[] ans = new int[n+1];
        ans[0] = 0;
        ans[1] = 0;
        ans[2] = Math.max(0, p[1]-p[0]-fee);
        for(int i = 3; i <= n; i++) {
            ans[i] = ans[i-1];
            for(int j = i-1; j>0; j--) {
                ans[i] = Math.max(ans[i], p[i-1] - p[j-1] + ans[j] - fee);
            }
        }
        return ans[n];
    }

    public int maxProfitDP(int[] p, int fee) {
        if(p == null) {
            return 0;
        }
        int n = p.length;
        if( n == 0 || n == 1) {
            return 0;
        }
        long[][] ans = new long[n+1][2];
        ans[0][0] = 0;
        ans[0][1] = Integer.MIN_VALUE;
        for(int i = 1; i <= n; i++) {
            ans[i][0] = Math.max(ans[i-1][0], ans[i-1][1] + p[i-1] - fee);
            ans[i][1] = Math.max(ans[i-1][1], ans[i-1][0] - p[i-1]);
        }
        long ret = Math.max(ans[n][0], ans[n][1]);
        if(ret < 0 ) {
            return 0;
        }
        return (int)ret;
    }
}
/*
Since it is an optimization max problem, let us define a method called max profit which takes in an input n and returns us the max profit till ith day. Now, to satisfy our method, we would have to sell the stock on the ith day. Else our profit will be the same as the n-1th day. Thus, if we don't sell the stock on the ith day, we get:-
F(n) = F(n-1).

However, if we do sell at the ith day:-
Our final answer will be dependent on the day we buy the stock. So, we can buy stock on any day whose price is less than the current day's price and in which case our final profit - transaction fee is positive. Thus,
F(n) = (p[n-1] - p[i]) +  (F[i] - fee)
such that the overall F(n) is positive.

If we don't do anything on that day:-
We just call the method for i-1.

Base cases:-
if i == 0
return 0;
if i == 1
return max(0, price[0] - fee)

Note that though this will give us the solution, but it will be O(n^2). We need an O(n) solution for these questions. We do that using the idea from the following article:-
https://discuss.leetcode.com/topic/107998/most-consistent-ways-of-dealing-with-the-series-of-stock-problems
 */
