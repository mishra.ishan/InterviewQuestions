package RecursionAndDp;

import java.util.Arrays;

//https://leetcode.com/problems/flood-fill/description/
public class FloodFill {
    public static void main(String[] args) {
        int[][] arr = {{3, 1, 1},
                {1, 3, 1},
                {1, 3, 1}
        };
        FloodFill floodFill = new FloodFill();
        arr = floodFill.floodFill(arr, 1,1,2);
        for(int i = 0; i < arr.length; i++) {
            System.out.println(Arrays.toString(arr[i]));
        }
    }

    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        if(image.length == 0) {
            return image;
        }
        int[][] ans = new int[image.length][image[0].length];
        Boolean[][] isVisited = new Boolean[image.length][image[0].length];
        for(int i = 0; i < image.length; i++) {
            Arrays.fill(isVisited[i], false);
        }
        floodFillHelper(image, sr, sc, image.length, image[0].length, newColor, image[sr][sc], ans, isVisited);
        ans[sr][sc] = newColor;
        for(int i = 0; i < image.length; i++) {
            for(int j = 0; j < image[i].length; j++) {
                if(ans[i][j] != newColor) {
                    ans[i][j] = image[i][j];
                }
            }
        }
        return ans;
    }

    private boolean isValid (int[][] arr, int x, int y, int rowLimit, int colLimit, int no, Boolean[][] isVisited) {
        return x >= 0 && y >= 0 && x < rowLimit && y < colLimit && !isVisited[x][y] && arr[x][y] == no;
    }

    private void floodFillHelper (int[][] arr, int x, int y, int rowLimit, int colLimit, int no, int old, int[][] ans, Boolean[][] isVisited) {
        if(isValid(arr, x+1, y, rowLimit, colLimit, old, isVisited)) {
            isVisited[x+1][y] = true;
            ans[x+1][y] = no;
            floodFillHelper(arr, x+1,y, rowLimit, colLimit, no, old, ans, isVisited);
        }
        if(isValid(arr, x, y+1, rowLimit, colLimit, old, isVisited)) {
            isVisited[x][y+1] = true;
            ans[x][y+1] = no;
            floodFillHelper(arr, x,y+1, rowLimit, colLimit, no, old, ans, isVisited);
        }
        if(isValid(arr, x-1, y, rowLimit, colLimit, old, isVisited)) {
            isVisited[x-1][y] = true;
            ans[x-1][y] = no;
            floodFillHelper(arr, x-1,y, rowLimit, colLimit, no, old, ans, isVisited);
        }
        if(isValid(arr, x, y-1, rowLimit, colLimit, old, isVisited)) {
            isVisited[x][y-1] = true;
            ans[x][y-1] = no;
            floodFillHelper(arr, x,y-1, rowLimit, colLimit, no, old, ans, isVisited);
        }
    }
}
