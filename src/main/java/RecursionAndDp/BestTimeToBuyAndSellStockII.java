//https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/description/
package RecursionAndDp;

public class BestTimeToBuyAndSellStockII {
    public static void main(String[] args) {
        int[] p = {1, 2, 7, 5, 9};
        BestTimeToBuyAndSellStockII bestTimeToBuyAndSellStockII = new BestTimeToBuyAndSellStockII();
        System.out.println(bestTimeToBuyAndSellStockII.maxProfitDP(p));
    }

    public int maxProfitDP(int[] p) {
        if(p == null) {
            return 0;
        }
        int n = p.length;
        if(n == 0 || n == 1) {
            return 0;
        }
        long[][] ans = new long[n+1][2];
        ans[0][0] = 0;
        ans[0][1] = Integer.MIN_VALUE;
        for(int i = 1; i <= n ; i++) {
            ans[i][0] = Math.max(ans[i-1][0], ans[i-1][1] + p[i-1]);
            ans[i][1] = Math.max(ans[i-1][1], ans[i-1][0] - p[i-1]);
        }
        return (int)ans[n][0];
    }
}
