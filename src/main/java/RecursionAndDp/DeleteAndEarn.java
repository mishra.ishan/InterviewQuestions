package RecursionAndDp;

//https://leetcode.com/problems/delete-and-earn/description/
public class DeleteAndEarn {
    public static void main (String[] args) {
        DeleteAndEarn deleteAndEarn = new DeleteAndEarn();
        int[] p = {3, 4, 2};
//        System.out.println(deleteAndEarn.maxPointPossibleRec(p.length, p));
        int max = Integer.MIN_VALUE;
        for(int val : p) {
            if(val > max) {
                max = val;
            }
        }
        int[] arr = new int[max+1];
        for(int i = 0; i < p.length; i++) {
            arr[p[i]] += p[i];
        }
        System.out.println(deleteAndEarn.maxPossibleRec(arr.length, arr));
        System.out.println(deleteAndEarn.maxPossibleDP(arr));
    }

    public int maxPointPossibleRec (int n, int[] p) {
        if(n == 0) {
            return 0;
        }
        int[] altArr = new int[n];
        int ans = maxPointPossibleRec(n-1, p);
        System.arraycopy(p, 0, altArr, 0, n);
        int valueToBeAdded = p[n-1];
        if(valueToBeAdded == Integer.MIN_VALUE) {
            return ans;
        }
        altArr[n-1] = Integer.MIN_VALUE;
        for(int i = 0; i<n-1; i++) {
            if(altArr[i] == valueToBeAdded - 1 || altArr[i] == valueToBeAdded + 1) {
                altArr[i] = Integer.MIN_VALUE;
            }
        }
        ans = Math.max(ans, valueToBeAdded + maxPointPossibleRec(n-1, altArr));
        return ans;
    }
    public int maxPossibleRec(int n, int[] p) {
        if(n == 0) {
            return 0;
        }
        if(n == 1) {
            return p[0];
        }
        int ans = maxPossibleRec(n-1, p);
        ans = Math.max(ans, p[n-1] + maxPossibleRec(n-2, p));
        return ans;
    }

    public int maxPossibleDP(int[] p) {
        if(p == null) {
            return 0;
        }
        int n = p.length;
        if(n == 0) {
            return 0;
        }
        int[] ans = new int[n+1];
        ans[0] = 0;
        ans[1] = p[0];
        for(int i = 2; i <= n; i++) {
            ans[i] = Math.max(ans[i-1], p[i-1] + ans[i-2]);
        }
        return ans[n];
    }
}

/*
Since it is an optimization maximization problem, let us define a method which takes a parameter n and the array p. Now, we can either choose the element, or reject the element. We can have two copies of the array. One unaltered in this iteration and the other altered one.

If we reject the element, then we simply go backward to n-1th term (given it is not already deleted). So,
f(n, origArr) = f(n-1, origArr)

If we choose the element, then our value increase by p[n-1] and we trace the entire array p and delete all elements with value p[n-1]-1 and p[n-1]+1 and then we call the n-1th term.
So, valueAdd = p[n-1]
delete All Elements With Value P[n-1]-1 and P[n-1]+1.
f(n, origArr) = max(valueAdd + f(n-1, altArr), f(n-1, origArr))


Now, the above will give us a solution, but it surely wastes up a lot of space and takes up a lot of time (every iteration demands us to copy the array). So, there must be a better way than that. Whenever we face problem like this, just think of all the problems that you have done (the 8 problems in that course) and think if it falls in any of those categories. Remember that in pair chain problem, first we had to first sort the input? Thus, what we can learn from it is that we would need to re arrange the given array to us in some way to make it fall under any of our known domains. Hence, let us say that at pos[p[n-1]], we are storing the value we will obtain if we take that element. Since we know that p[n-1] can be max of 10,000, hence we create an array, say arr, in which, the ith element tells us the value we will obtain if we delete that element.
That is, for(int val : p) {
    arr[val] += val;
}

Thus, an array p of (3,4,2,2,2) will look like:-
arr: (0,0,6,3,4,......)

Also, an array p of (2, 2, 3, 3, 3, 4) will look like:-
arr: (0,0,4,9,4)

Now we see that this has become our house robber problem. If we rob the nth house, we can't rob the n-1th house. Thus, we can solve it that way.
 */
