package RecursionAndDp.IntroductionToDPUdemy;

import java.util.Arrays;
import java.util.Comparator;

class Pair {
    int firstElement;
    int secondElement;

    public Pair(int firstElement, int secondElement) {
        this.firstElement = firstElement;
        this.secondElement = secondElement;
    }
}

public class MaximumLengthPairChain {
    public static void main(String... args) {
        MaximumLengthPairChain maximumLengthPairChain = new MaximumLengthPairChain();
        Pair p1 = new Pair(1,2);
        Pair p2 = new Pair(2,3);
        Pair p3 = new Pair(3,4);
        Pair[] p = {p1, p2, p3};
        System.out.println(maximumLengthPairChain.findMaxLengthPairChainRec(3, p));
        System.out.println(maximumLengthPairChain.findMaxLengthPairDP(p));
    }

    public int findMaxLengthPairChainRec (int n, Pair[] p) {
        if(n < 1) {
            return 0;
        }
        if(n == 1) {
            return 1;
        }
        Arrays.sort(p, new Comparator<Pair>() {
            @Override
            public int compare(Pair o1, Pair o2) {
                if(o1.firstElement < o2.firstElement) {
                    return -1;
                }
                if(o1.firstElement == o2.firstElement) {
                    return 0;
                }
                return 1;
            }
        });
        int firstElementOfNthPair = p[n-1].firstElement;
        int ans = findMaxLengthPairChainRec(n-1, p);
        for(int i = 0; i < n-1; i++) {
            if(p[i].secondElement < firstElementOfNthPair) {
                ans = Math.max(ans, 1 + findMaxLengthPairChainRec(i+1, p));
            }
        }
        return ans;
    }

    public int findMaxLengthPairDP (Pair[] p) {
        int n = p.length;
        if(n < 2) {
            return n;
        }
        int[] ans = new int[n];
        ans[0] = 1;
        Arrays.sort(p, new Comparator<Pair>() {
            @Override
            public int compare(Pair p1, Pair p2) {
                if(p1.firstElement < p2.firstElement) {
                    return -1;
                }
                if(p1.firstElement == p2.firstElement) {
                    return 0;
                }
                return 1;
            }
        });
        for(int i = 1; i < n; i++) {
            ans[i] = ans[i-1];
            for(int j = 0; j < i; j++) {
                if(p[j].secondElement < p[i].firstElement) {
                    ans[i] = Math.max(ans[i], 1 + ans[j]);
                }
            }
        }
        return ans[n-1];
    }
}
