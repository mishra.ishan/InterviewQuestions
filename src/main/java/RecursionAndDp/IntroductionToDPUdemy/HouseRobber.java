package RecursionAndDp.IntroductionToDPUdemy;

public class HouseRobber {
    public static void main(String[] args) {
        int[] a = {6,3,7,5,8,9,11,23,5,2,10};
        HouseRobber houseRobber = new HouseRobber();
        System.out.println(houseRobber.RobRec(a, a.length));
        System.out.println(houseRobber.RobDP(a));
    }

    public int RobRec(int[] nums, int n) {
        if(n==0) {
            return 0;
        }
        if(n==1) {
            return nums[0];
        }
        //calculating max(Rob(0),Rob(1),Rob(2),Rob(3),Rob(4),.....,Rob(n-2))
        int max_val = -1;
        //VERY IMPORTANT THE USE OF EQUAL TO I.E., J < N-2 WONT GIVE THE ANSWER, BUT J <= N-2 WILL.
        //WHY? Cause if we are doing j < n-2, then we are skipping the house no n-2 (i.e., nums[n-2-1]). SO WE NEED TO INCLUDE IT
        for(int j=0; j<=n-2; j++) {
            max_val = Integer.max(max_val, RobRec(nums, j));
        }
        //final answer will be f(n) = max(Rob(n-1)(we didn't rob the rouse), nums[n] + max(Rob(0), Rob(1), Rob(2), Rob(3),...... Rob(n-2)))
        return Integer.max(RobRec(nums, n-1), nums[n-1] + max_val);
    }

    public int RobDP(int[] nums) {
        int n = nums.length;
        int[] ans = new int[n+1];
        ans[0] = 0;
        ans[1] = nums[0];
        for(int i=2; i<=n; i++) {
            ans[i] = Integer.max(ans[i-1], nums[i-1] + ans[i-2]);
        }
        return ans[n];
    }
}
/*
    It is also a max/min opt problem, hence we would need to explore all the cases. So, we have an array which contains the price that we will get by looting the house.
    So, we will have two cases:-
    1. Either we loot the house.
    2. We don't loot the house.
    We would just need to take out the max among the two. So let us say create a method which does exactly this only. That is, we call the method on the given array and a number (say n),
    So our method will return us the max money possible for the robber if there were those n houses.
    So, our rec solution will be:-
    f(n) = max(f(n-1)(we didn't rob the rouse), nums[n] + max(f(0),f(1),f(2),f(3),f(4),.....,f(n-2))
    Here, our base cases will be:- f(0) = 0, f(1) = nums[0] (if only one house present, we will loot it), f(2) = max(nums[0], nums[1])
 */