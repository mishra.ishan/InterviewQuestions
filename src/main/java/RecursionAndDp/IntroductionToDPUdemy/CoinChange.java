package RecursionAndDp.IntroductionToDPUdemy;

//https://leetcode.com/problems/coin-change/description/
public class CoinChange {
    public static void main(String[] args) {
        int[] p = {1, 2, 5};
        int amount = 99;
        CoinChange coinChange = new CoinChange();
        long minNoOfCoinsRec = coinChange.minNoOfCoinsNeededRec(p, amount, p.length);
        if(minNoOfCoinsRec == Integer.MAX_VALUE) {
            minNoOfCoinsRec = -1;
        }
        long minNoOfCoinsDP = coinChange.minNoOfCoinsNeededDP(p, amount);
        if(minNoOfCoinsDP == Integer.MAX_VALUE) {
            minNoOfCoinsDP = -1;
        }
        System.out.println(minNoOfCoinsRec);
        System.out.println(minNoOfCoinsDP);
    }

    public long minNoOfCoinsNeededRec (int [] p, int amount, int n) {
        if(amount < 0 || n <= 0) {
            return Integer.MAX_VALUE;
        }
        if (amount == 0) {
            return 0;
        }
        long ans = minNoOfCoinsNeededRec(p, amount, n-1);
        ans = Math.min(ans, 1 + Math.min(minNoOfCoinsNeededRec(p, amount - p[n-1], n-1), minNoOfCoinsNeededRec(p, amount - p[n-1], n)));
        return ans;
    }

    public long minNoOfCoinsNeededDP (int[] p, int amount) {
        int n = p.length;
        long[][] ans = new long[n+1][amount+1];
        for(int i=0; i<=n; i++) {
            for(int j=0; j<=amount; j++) {
                if(i == 0) {
                    ans[i][j] = Integer.MAX_VALUE;
                }
                else if(j == 0) {
                    ans[i][j] = 0;
                }
                else {
                    ans[i][j] = ans[i-1][j];
                    if(j - p[i-1] >= 0) {
                        ans[i][j] = Math.min(ans[i][j], 1 + Math.min(ans[i-1][j-p[i-1]], ans[i][j-p[i-1]]));
                    }
                }
            }
        }
        return ans[n][amount];
    }
}

/*
    As we can see, it is also a minimization optimization problem. So, we need to make it into sub problems. Let's say we have a method fn which takes in the input as the array and the amount and returns the min no of coins needed for the same (or -1).

    So, we need to think that we need to take the nth coin or not.

    If we take :-
    then our amount will reduce to amount - p[n-1]. Also, we can add 1 to our final answer. Note that we won't send our method backwards, but instead just reduce the amount and call it for the same coin too (cause we are allowed to take the same coin again).
    So, in that case, our method will be
    fn(amount,n) = min(1 + min(fn(amount-p[n-1], n-1), fn(amount-p[n-1], n));

    If we don't take the nth coin:-
     then we would need to call the method for n-1th coin.
     So, in that case, amount shall be:-
     fn(amount, n) = fn(amount, n-1);

     So, we need to take out the min of the two. Also, if the amount reaches less then zero, then we return Integer.MAX_INT to show that it is not possible to have a solution with this combination of coins.
     */