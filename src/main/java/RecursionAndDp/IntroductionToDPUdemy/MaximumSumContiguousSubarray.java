package RecursionAndDp.IntroductionToDPUdemy;

//https://leetcode.com/problems/maximum-subarray/description/
public class MaximumSumContiguousSubarray {
    public static void main(String[] args) {
        MaximumSumContiguousSubarray maximumSumContiguousSubarray = new MaximumSumContiguousSubarray();
        int[] p = {-2,-1};
        System.out.println(maximumSumContiguousSubarray.maxContSubarraySumDP(p));
    }

    public int maxContSubarraySumDP (int[] p) {
        if(p == null) {
            return 0;
        }
        int n = p.length;
        if(n == 0) {
            return 0;
        }
        int[] ans = new int[n+1];
        ans[0] = 0;
        ans[1] = p[0];
        if(n == 1) {
            return p[0];
        }
        int max_sum = p[0];
        for(int i = 2; i <= n; i++) {
            ans[i] = Math.max(p[i-1], p[i-1] + ans[i-1]);
            if(ans[i] > max_sum) {
                max_sum = ans[i];
            }
        }
        return max_sum;
    }
}

/*
Since somehow we need to connect taking the current input with the n-1th recursion, in this question (just like in longest common substring), we say that our method returns us the maximum subarray formed ending at the nth element. So, till now we have two types of method declarations for DP. One would want you to return the answer when called for the nth element. Other (like longest common substring and this one) would want you to return the maximum among the entire array of values.
 */