package RecursionAndDp.IntroductionToDPUdemy;

public class ClimbingStairs {
    public static void main(String[] args) {

    }

    public static int twoAnsDP (int n) {
        int[] s = new int[n];
        s[1] = 1;
        s[2] = 2;
        for(int i=3; i<=n; i++) {
            s[i] = s[i-1] + s[i-2];
        }
        return s[n];
    }

    public static int threeAnsDP (int n) {
        int[] s = new int[n];
        s[1] = 1;
        s[2] = 2;
        s[3] = 4;
        for (int i=4; i<=n; i++) {
            s[i] = s[i-1] + s[i-2] + s[i-3];
        }
        return s[n];
    }
}
