package RecursionAndDp.IntroductionToDPUdemy;

public class Knapsack {
    public  static void main(String[] args) {
        int[] price = {60,100,120};
        int[] weight = {10,20,30};
        Knapsack knapsack = new Knapsack();
        System.out.println(knapsack.knapsack(price.length, 50, weight, price));
        System.out.println(knapsack.knapsackDP(weight, price, 50));
    }

    /*
    This method returns the max price possible for the robber
     */
    public int knapsack (int n, int w, int[] weight, int[] price) {
        if(w <= 0 || n<=0) {
            return 0;
        }
        int ans = knapsack(n-1, w, weight, price);
        ans = Math.max(ans, price[n-1] + knapsack(n-1, w-weight[n-1], weight, price));
        return ans;
    }

    public int knapsackDP(int[] weight, int[] price, int w) {
        int n = weight.length;
        if(n == 0) {
            return 0;
        }
        int[][] ans = new int[n+1][w+1];
        for(int i=0; i<= n; i++) {
            for(int j=0; j<=w; j++) {
                if(i == 0 || j == 0) {
                    ans[i][j] = 0;
                }
                else {
                    ans[i][j] = ans[i - 1][j];
                    if (j - weight[i-1] >= 0) {
                        ans[i][j] = Math.max(ans[i - 1][j], price[i-1] + ans[i - 1][j - weight[i-1]]);
                    }
                }
            }
        }
        return ans[n][w];
    }
}
