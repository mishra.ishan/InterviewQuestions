package RecursionAndDp.IntroductionToDPUdemy;

public class LongestCommonSubstring {
    public static void main(String[] args) {
        LongestCommonSubstring lcstr = new LongestCommonSubstring();
        String x = "abcdef";
        String y = "mnabdc";
        System.out.println(lcstr.lcstrDP(x.toCharArray(), y.toCharArray(), x.length(), y.length()));
    }

    public int lcstrDP (char[] x, char[] y, int m, int n) {
        if(m == 0 || n == 0) {
            return 0;
        }
        int[][] ans = new int[m+1][n+1];
        int maxTillNow = 0;
        for(int i = 0; i<=m; i++) {
            for(int j = 0; j<=n; j++) {
                if(i == 0 || j == 0) {
                    ans[i][j] = 0;
                }
                else {
                    if (x[i-1] == y[j-1]) {
                        ans[i][j] = ans[i - 1][j - 1] + 1;
                        if(ans[i][j] > maxTillNow) {
                            maxTillNow = ans[i][j];
                        }
                    }
                    else {
                        ans[i][j] = 0;
                    }
                }
            }
        }
        return maxTillNow;
    }
}
