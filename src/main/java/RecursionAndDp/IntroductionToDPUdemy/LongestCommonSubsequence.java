package RecursionAndDp.IntroductionToDPUdemy;

public class LongestCommonSubsequence {
    public static void main(String[] args) {
        String a = "abacad";
        String b = "abcaaaaad";
        LongestCommonSubsequence lcs = new LongestCommonSubsequence();
        System.out.println(lcs.lcs(a.toCharArray(), b.toCharArray(), a.length(), b.length()));
        System.out.println(lcs.lcsDP(a.toCharArray(), b.toCharArray(), a.length(), b.length()));
    }

    public int lcs (char[] a, char[] b, int i, int j) {
        if(i <= 0 || j <= 0) {
            return 0;
        }
        if(a[i-1] == b[j-1]) {
            return 1 + lcs(a,b,i-1,j-1);
        }
        return Math.max(lcs(a,b,i-1,j), lcs(a,b,i,j-1));
    }

    public int lcsDP (char[] a, char[] b, int m, int n) {
        if(m == 0 || n == 0) {
            return 0;
        }
        int[][] ans = new int[m+1][n+1];
        for(int i = 0; i <= m; i++) {
            for(int j = 0; j <= n; j++) {
                if(i == 0 || j == 0) {
                    ans[i][j] = 0;
                }
                else {
                    if(a[i-1] == b[j-1]) {
                        ans[i][j] = 1 + ans[i-1][j-1];
                    }
                    else {
                        ans[i][j] = Math.max(ans[i-1][j], ans[i][j-1]);
                    }
                }
            }
        }
        return ans[m][n];
    }
}
