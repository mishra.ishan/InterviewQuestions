package RecursionAndDp.IntroductionToDPUdemy;

import java.util.Arrays;

public class CuttingRod {
    public static void main(String[] args) {
        int[] a = {1,5,8,9,10};
        CuttingRod cuttingRod = new CuttingRod();
        System.out.println(cuttingRod.maxPricePossibleRec(a, a.length));
        System.out.println(cuttingRod.maxPossiblePriceDP(a));
    }

    public int maxPricePossibleRec (int[] nums, int n) {
        if(n == 0) {
            return 0;
        }
        int max = -1;
        for(int i=0; i<n; i++) {
            max = Integer.max(max, nums[i] + maxPricePossibleRec(nums, n - i - 1));
        }
        return max;
    }

    public int maxPossiblePriceDP (int[] nums) {
        int n = nums.length;
        int[] ans = new int[n + 1];
        Arrays.fill(ans, -1);
        ans[0] = 0;
        for(int i = 1; i <= n; i++) {
            for(int j = 0; j<i; j++) {
                ans[i] = Integer.max(ans[i], nums[j] + ans[i-j-1]);
            }
        }
        return ans[n];
    }

    /*
    It is a maximization/minimization optimization problem. Hence we would need to check for all the possible cases.
    So, we just cut one piece and check what is the optimal division price for the remaining n-1 elements,
    OR, we can cut two piece and check what is the optimal division price for the remaining n-2 elements
    OR, we can cut three pieces and check what is the optimal division price for the remaining n-3 elements and so on...
    Hence, the recursive solution will be:- f(n) = max(num[0] + f(n-1), num[1] + f(n-2), num[2] + f(n-3), ........, num[n-1])
    Here base case will be that f(0) will be 0.
    To convert it into DP, we simply just memoize it. Build it up from bottom up. That is, if we know the solution to f(3),
    then we can find optimal division price for f(4) by f(4) = max(nums[0] + f(3), nums[1] + f(2), nums[2] + f(3), nums[3])
     */
}
