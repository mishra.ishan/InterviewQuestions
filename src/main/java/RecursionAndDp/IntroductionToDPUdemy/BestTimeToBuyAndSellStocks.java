package RecursionAndDp.IntroductionToDPUdemy;

public class BestTimeToBuyAndSellStocks {
    public static void main(String[] args) {
        int[] price = {7,1,5,3,6,4};
        BestTimeToBuyAndSellStocks bestTimeToBuyAndSellStocks = new BestTimeToBuyAndSellStocks();
        System.out.println(bestTimeToBuyAndSellStocks.maxStockSellPrice(price, price.length));
        System.out.println(bestTimeToBuyAndSellStocks.maxStockPricePossibleDP(price));
    }

    public int maxStockSellPrice (int[] price, int n) {
        if(n == 0) {
            return 0;
        }
        int maxPrice = maxStockSellPrice(price, n-1);
        for(int i=n-2; i>=0; i--) {
            maxPrice = Math.max(maxPrice, price[n-1] - price[i]);
        }
        return maxPrice;
    }

    public int maxStockPricePossibleDP(int[] price) {
        int n = price.length;
        if(n==0) {
            return 0;
        }
        int[] maxPrice = new int[n+1];
        maxPrice[0] = 0;
        int min = price[0];
        for(int i = 1; i<=n; i++) {
            min = Math.min(min, price[i-1]);
            maxPrice[i] = Math.max(maxPrice[i-1], price[i-1] - min);
        }
        return maxPrice[n];
    }
}
