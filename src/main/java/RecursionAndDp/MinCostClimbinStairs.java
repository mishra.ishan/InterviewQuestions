package RecursionAndDp;

//https://leetcode.com/problems/min-cost-climbing-stairs/description/
public class MinCostClimbinStairs {
    public static void main(String[] args) {
        int[] arr = {1, 100, 1, 1, 1, 100, 1, 1, 100, 1};
        MinCostClimbinStairs minCostClimbinStairs = new MinCostClimbinStairs();
        System.out.println(minCostClimbinStairs.minCostStepRec(arr.length+1, arr));
        System.out.println(minCostClimbinStairs.minCostStepDP(arr));
    }

    public int minCostStepRec (int n, int[] p) {
        if(n <= 2) {
            return 0;
        }
        return Math.min(p[n-2] + minCostStepRec(n-1, p), p[n-3] + minCostStepRec(n-2, p));
    }

    public int minCostStepDP(int[] p) {
        if(p == null) {
            return 0;
        }
        int n = p.length;
        if(n == 1) {
            return 0;
        }
        int[] ans = new int[n+2];
        ans[0] = 0;
        ans[1] = 0;
        ans[2] = 0;
        for(int i=3; i<=n+1; i++) {
            ans[i] = Math.min(p[i-2] + ans[i-1], p[i-3] + ans[i-2]);
        }
        return ans[n+1];
    }

    /*
    Since it is a minimization optimization problem, we can use DP in this scenario. Let's say that we have a method that takes an input n and returns the min cost needed to get to that step. So, we need to think how can we reach that stair. We can either reach the stair from n-1th stair or n-2th stair.
    If we take n-1th stair, then we would have to give the cost of n-1th stair to climb one step from it.
    If we take n-2th stair, then we would have to give the cost of n-2th stair to climb two steps from it.

    For e.g., ......., 20, 28, <destination>

    So, we can reach the destination by :-
    1. Reaching 20 in minimum cost and then adding 20 to our final cost and jump two steps.
    2. Reaching 28 in minimum cost and then adding 28 to our final cost and jump one step.

    The thing to notice here is that if we have an array of 3 numbers, say 10,15,20, then we need to reach the 4th step. So we would need to call the method as f(n+1). Also note that the base cases will be that to reach the 1st or 2nd stair, no cost would be needed, since we would be starting from step 0 and no cost is needed to pass that step. So we can directly jump to either 1st or 2st step without including any cost.

    So, our final recursion will be:-
    f(n) = min(f(n-1) + <cost to jump the n-1th step>, f(n-2) + <cost to jump the n-2th step>)
     */
}
