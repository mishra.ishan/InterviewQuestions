package RecursionAndDp;


import java.util.ArrayList;
import java.util.List;

public class ExpressionAddOperators {
    public static void main (String[] args) {

    }

    public List<String> addOperators(String num, int target) {
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < num.length(); i++) {
            sb.append(num.charAt(i));
        }
        List<String> ans = new ArrayList<>();
        for(int i = 0; i < num.length(); i++) {
            fn(sb.append(sb.charAt(0)), ans, target, 0, 0);
            sb.deleteCharAt(0);
        }
        return ans;
    }

    public void fn (StringBuffer num, List<String> ans, int target, int i, int sumTillNow) {
        if(i == num.length()) {
            //TODO check if the final sum is equal to target
            return;
        }
        int digit = num.charAt(i) - '0';
        fn(num, ans, target, i+1, sumTillNow * digit);
        fn(num, ans, target, i+1, sumTillNow + digit);
        fn(num, ans, target, i+1, sumTillNow - digit);
    }
}
