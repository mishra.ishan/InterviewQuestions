package RecursionAndDp;

import java.util.Arrays;
import java.util.Comparator;

//https://leetcode.com/problems/maximum-length-of-pair-chain/description/
public class LongestChainPair {

    public static void main (String[] args) {
        LongestChainPair longestChainPair = new LongestChainPair();
        int[][] a = {{1,2}, {2,3}, {3,4}};
        System.out.println(longestChainPair.findLongestChain(a));
    }

    public int findLongestChain(int[][] pairs) {
        if(pairs == null || pairs.length == 0) {
            return 0;
        }
        int n = pairs.length;
        if (n == 1) {
           return 1;
        }
        Arrays.sort(pairs, new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                if(o1[0] < o2[0]) {
                    return -1;
                }
                if(o1[0] == o2[0]) {
                    return 0;
                }
                return 1;
            }
        });
        int[] ans = new int[n];
        ans[0] = 1;
        for(int i = 1; i<n; i++) {
            ans[i] = ans[i-1];
            for(int j = 0; j<i; j++) {
                if(pairs[j][1] < pairs[i][0]) {
                    ans[i] = Math.max(ans[i], 1 + ans[j]);
                }
            }
        }
        return ans[n-1];
    }
}
