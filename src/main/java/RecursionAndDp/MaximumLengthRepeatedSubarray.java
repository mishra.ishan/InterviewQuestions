package RecursionAndDp;

//https://leetcode.com/problems/maximum-length-of-repeated-subarray/description/
public class MaximumLengthRepeatedSubarray {

    public static void main(String[] args) {
        int[] a = {1,2,3,2,1};
        int[] b = {3,2,1};
        MaximumLengthRepeatedSubarray maximumLengthRepeatedSubarray = new MaximumLengthRepeatedSubarray();
        System.out.println(maximumLengthRepeatedSubarray.maxLengthRepeatedSubarray(a,b));
    }

    public int maxLengthRepeatedSubarray(int[] a, int[] b) {
        if(a == null || b == null) {
            return 0;
        }
        int m = a.length, n = b.length;
        if(m == 0 || n == 0) {
            return 0;
        }
        int[][] ans = new int[m+1][n+1];
        int maxVal = 0;
        for(int i = 0 ; i<= m; i++) {
            for(int j = 0; j <= n; j++) {
                if(i == 0 || j == 0) {
                    ans[i][j] = 0;
                }
                else {
                    if(a[i-1] == b[j-1]) {
                        ans[i][j] = 1 + ans[i-1][j-1];
                        if(ans[i][j] > maxVal) {
                            maxVal = ans[i][j];
                        }
                    }
                    else {
                        ans[i][j] = 0;
                    }
                }
            }
        }
        return maxVal;
    }
}
