package RecursionAndDp;

import java.util.HashMap;
import java.util.Map;

public class PowerSet {
    public static void main(String[] args){
        PowerSet powerSet = new PowerSet();
        String orig = "abc";
        powerSet.printPowerSet(orig, new StringBuffer(), 0, orig.length(), 0);
    }

    private void printPowerSet(String orig, StringBuffer str, int i, int n, int strLen) {
        if(i==n) {
            System.out.println(str.toString());
            return;
        }
        printPowerSet(orig, str.append(orig.charAt(i)), i+1, n, strLen+1);
        int len = str.length()-1;
        if(len >= 0)
        str.deleteCharAt(len);
        printPowerSet(orig, str, i+1, n, strLen);
    }
}
