package RecursionAndDp;

public class EditDistance {

    private Integer[][] table;

    public EditDistance(int m, int n) {
        table = new Integer[m][n];
    }

    public static void main(String[] args) {
        String src = "zvfrkmlnozjkpqpxrjxkitzyxacbhhkicqcoendtomfgdwdwfcgpxiqvk";
        String dest = "uytdlcgdewhtaciohordtqkvwcsgspqoqmsboaguwnny";
        EditDistance ed = new EditDistance(src.length(), dest.length());
        long startTime = System.currentTimeMillis();
        System.out.println("top down: "+ed.editDistance(src, dest) + " time: "+(System.currentTimeMillis() - startTime));
        startTime = System.currentTimeMillis();
        System.out.println("Bottom up: "+ed.editDistanceBottomUp(src, dest)+" time: "+(System.currentTimeMillis() - startTime));
    }

    public int editDistance(String source, String dest) {
        int n1 = source.length();
        int n2 = dest.length();
        if(n1 == 0) {
            return n2;
        }
        if(n2 == 0) {
            return n1;
        }
        return editDistanceHelper(source, 0, n1, dest, 0, n2);
    }

    private int editDistanceHelper(String source, int i, int n1, String dest, int j, int n2) {
        if(i == n1) {
            return n2 - j;
        }
        if(j == n2) {
            return n1 - i;
        }
        if(table[i][j] != null) {
            return table[i][j];
        }
        if(source.charAt(i) == dest.charAt(j)) {
            table[i][j] = editDistanceHelper(source, i+1, n1, dest, j+1, n2);
        }
        else{
            table[i][j] = Math.min(Math.min(1 + editDistanceHelper(source, i+1, n1, dest, j, n2),
                    1 + editDistanceHelper(source, i, n1, dest, j+1, n2)),
                    1 + editDistanceHelper(source, i+1, n1, dest, j+1, n2));
        }
        return table[i][j];
    }

    public int editDistanceBottomUp(String a, String b) {
        int m = a.length(), n = b.length();
        int[][] dp = new int[m+1][n+1];
        for(int i=0; i<=m; i++) {
            dp[i][0] = i;
        }
        for(int j=0; j<=n; j++) {
            dp[0][j] = j;
        }
        for(int i=1; i<=m; i++) {
            for(int j=1; j<=n; j++) {
                int minOfThree = Math.min(dp[i-1][j-1],
                        Math.min(dp[i][j-1], dp[i-1][j]));
                if(a.charAt(i-1) == b.charAt(j-1)) {
                    dp[i][j] = dp[i-1][j-1];
                }
                else {
                    dp[i][j] = minOfThree + 1;
                }
            }
        }
        return dp[m][n];
    }
}
