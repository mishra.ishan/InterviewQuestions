package RecursionAndDp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ishan.mishra on 8/28/17.
 */
public class RestoreIPAddress {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        RestoreIPAddress restoreIPAddress = new RestoreIPAddress();
        System.out.println(restoreIPAddress.restoreIpAddresses(bufferedReader.readLine()));
    }

    public List<String> restoreIpAddresses(String s) {
        if(s.isEmpty()) {
            return new ArrayList<>();
        }
        Integer val = s.charAt(0) - 48;
        return fn (s, String.valueOf(val), 1, s.length(), 0, val);
    }

    private static List<String> fn(String s, String ipAddress, int i, int length, int k, int currVal) {
        if(ipAddress.contains("255.255.255")) {
            System.out.println();
        }
        if (i == length && k == 3 && currVal >= 0 && currVal <= 255) {
            List<String> ret = new ArrayList<>();
            ret.add(ipAddress);
            return ret;
        }
        if(i >= length || k > 3 || currVal > 255) {
            return new ArrayList<>();
        }
        Integer value = s.charAt(i) - 48;
        List<String> ans = new ArrayList<>();
        List<String> ret1 = fn(s, (ipAddress +  "." + String.valueOf(value)), i + 1, length, k + 1, value);
        List<String> ret2 = new ArrayList<>();
        if(currVal != 0) {
            ret2 = fn(s, ipAddress + String.valueOf(value), i + 1, length, k, currVal * 10 + value);
        }
        if(!ret1.isEmpty()) {
            ans.addAll(ret1);
        }
        if(!ret2.isEmpty()) {
            ans.addAll(ret2);
        }
        return ans;
    }

}
