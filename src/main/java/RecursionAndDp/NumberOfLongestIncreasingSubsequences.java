package RecursionAndDp;

import java.util.Arrays;

//https://leetcode.com/problems/number-of-longest-increasing-subsequence/description/
public class NumberOfLongestIncreasingSubsequences {

    public static void main(String[] args) {
        NumberOfLongestIncreasingSubsequences numberOfLongestIncreasingSubsequences = new NumberOfLongestIncreasingSubsequences();
        int[] p = new int[]{1,3,5,4,7};
        System.out.println(Arrays.toString(p));
        System.out.println(numberOfLongestIncreasingSubsequences.noOfLIS(p));
    }
    public int noOfLIS(int[] p) {
        if(p == null) {
            return 0;
        }
        int n = p.length;
        if(n == 0) {
            return 0;
        }
        int[] len = new int[n];
        int[] cnt = new int[n];
        len[0] = 1;
        for(int i = 1; i < n; i++) {
            int temp = 0;
            for(int j = i-1; j>=0; j--) {
                if(p[j] < p[i]) {
                    temp = Math.max(temp, len[j]);
                }
            }
            len[i] = temp + 1;
        }
        System.out.println(Arrays.toString(len));
//        Arrays.fill(cnt, 1);
        cnt [0] = 1;
        for(int i = 1; i < n; i++) {
            for(int j = i-1; j >= 0; j--) {
                if(p[i] > p[j]) {
                    if(len[i] - 1 == len[j]) {
                        cnt[i] += cnt[j];
                    }
                }
            }
            if(cnt[i] == 0) {
                cnt[i] = 1;
            }
        }
        System.out.println(Arrays.toString(cnt));
        int maxLen = 1;
        for(int i = 0; i < n; i++) {
            if(len[i] > maxLen) {
                maxLen = len[i];
            }
        }
        int ret = 0;
        for(int i = 0; i < n; i++) {
            if(len[i] == maxLen) {
                ret += cnt[i];
            }
        }
        return ret;
    }

}
/*
This question is similar to longest increasing subsequence. Here our main aim is to get an enumeration, i.e., no of LIS from 0 to n-1. So, in any case, we would need to take out the LIS array to know the longest increasing subsequence ending with a particular element. Now, in questions like these, which as we can see are extensions of our already solved problems, we can take a new array and store the real answer in that array.

So, we take an array cnt in which cnt[n] stores the number of LIS ending with p[n]. So how can we get that? We would take the help of our LIS array for this. We know that for any element j (smaller than i) with LIS[j] + 1 == LIS[i], the element j must form a part of the total number of LIS from 0 to n including p[n]. Thus, cnt[i] += cnt[j]. Now, multiple may have the same value. For e.g., when we run the array 1,3,5,4,7, we get the LIS array and count array as :-
ARR:[1, 3, 5, 4, 7]
LIS:[1, 2, 3, 3, 4]
CNT:[1, 1, 1, 1, 2]

So, as we can see, that count for 7 will add the count for 4 and 5. It is cause no of LIS till 5 (1,3,5) and till 4(1,3,5,4) are both 3, but LIS till 7 (1,3,5,4,7) is 4. Hence, LIS till 7 can be formed by both 5 as well as 4. Hence we take both the values. Also, if we don't find any value which is smaller than the element, then the count array value for that element will be 1.
*/
