package RecursionAndDp;

public class MinimumPartition {

    public static void main(String[] args) {
        int[] arr = {1,6,6,11};
        int sum = 0;
        for(int a : arr) {
            sum += a;
        }
        MinimumPartition minPart = new MinimumPartition();
        System.out.println(minPart.minimumBottomUp(arr));
    }

    public int minimumPartition(int[] arr) {
        return minimumHelper(arr, 0, arr.length, 0, 0);
    }

    private int minimumHelper(int[] arr, int i, int n, int sum1, int sum2) {
        if(i == n) {
            return Math.abs(sum1 - sum2);
        }
        return Math.min(minimumHelper(arr, i+1, n, sum1 + arr[i], sum2),
                minimumHelper(arr, i+1, n, sum1, sum2 + arr[i]));
    }

    public int minimumBottomUp(int[] arr) {
        int n = arr.length;
        if(n == 0) {
            return 0;
        }
        if(n==1) {
            return arr[0];
        }
        int sum = 0;
        for(int i=0 ; i<n; i++) {
            sum += arr[i];
        }
        boolean[][] dp = new boolean[n+1][sum+1];
        for(int i=0; i<=n; i++) {
            dp[i][0] = true;
        }
        for(int j=1; j<=sum; j++) {
            dp[0][j] = false;
        }
        for(int i=1; i<=n; i++) {
            for(int j=1; j<=sum; j++) {
                dp[i][j] = dp[i-1][j];      //excluding a[i-1]th element from S1 (and rather adding to other set), we might or might not get the sum j (that's why referring to the previous value.
                if(j >= arr[i-1]) {
                    dp[i][j] |= dp[i-1][j-arr[i-1]];    //including the a[i-1]th element in S1, we acheive sum j
                }
            }
        }
        int minDiff = Integer.MAX_VALUE;
        for(int j=sum/2; j>=0; j--) {
            if(dp[n][j] == true) {
                minDiff = sum - 2*j;
                break;
            }
        }
        return minDiff;
    }
}
