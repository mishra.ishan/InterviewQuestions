package RecursionAndDp;


public class LongestPalindromicSubstring {
    public static void main (String[] args) {
        String a = "abccccdd";
        LongestPalindromicSubstring lps = new LongestPalindromicSubstring();
        System.out.println(lps.longestPalindromicSubstring(a, 0, a.length() - 1));
    }

    public int longestPalindromicSubstring (String a, int i, int j) {
        if(i > j) {
            return 0;
        }
        if(i == j) {
            return 1;
        }
        if(a.charAt(i) == a.charAt(j)) {
            return 2 + longestPalindromicSubstring(a, i+1, j-1);
        }
        return Math.max(longestPalindromicSubstring(a, i, j-1), longestPalindromicSubstring(a, i+1, j));
    }
}
