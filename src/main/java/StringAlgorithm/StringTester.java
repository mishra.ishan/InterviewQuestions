package StringAlgorithm;

public class StringTester {
    public static void main(String[] args) {
//        String[] strArray = new String[]{"abc", "def"};
        Trie trie = new Trie();;
        trie.insert("ab");
        trie.insert("abc");
        trie.insert("cd");
        trie.insert("afg");
        trie.insert("asa");
        trie.insert("agg");
        trie.insert("aggdr");
        trie.insert("a");
        System.out.println(trie.getWords());
    }
}
