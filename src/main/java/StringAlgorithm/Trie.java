package StringAlgorithm;

import java.util.ArrayList;
import java.util.List;

public class Trie {
    Trie[] trie;
    boolean isLeaf;
    Character character;

    public Trie() {
        isLeaf = false;
        trie = new Trie[26];            //assuming only short characters
    }

    public void insert(String word) {

        Trie t = this;
        char c;
        for(int i=0; i<word.length(); i++) {
            c = word.charAt(i);
            int indx = c - 'a';
            if(t.trie[indx] == null) {
                t.trie[indx] = new Trie();
                t.trie[indx].character = c;
            }
            if(i == word.length() - 1) {
                t.trie[indx].isLeaf = true;
            }
            t = t.trie[indx];
        }
    }

    public List<String> getWords() {
        List<String> ans = new ArrayList<>();
        getAllWords(this, "", ans);
        return ans;
    }

    private void getAllWords(Trie t, String stringTillNow, List<String> finalStrings) {
        if(t.isLeaf) {
            finalStrings.add(stringTillNow);
        }
        for(int i=0; i<26; i++) {
            if(t.trie[i] != null) {
                getAllWords(t.trie[i], stringTillNow + (t.trie[i].character), finalStrings);
            }
        }
    }

    public boolean search(String word) {
        Trie t = this;
        for(int i=0; i<word.length(); i++) {
            int indx = word.charAt(i) - 'a';
            if(t.trie[indx] != null) {
                t = t.trie[indx];
            }
            else {
                return false;
            }
            if(i == word.length() - 1 && t.isLeaf) {
                return true;
            }
        }
        return false;
    }
}
