package Graph;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;

class Graph {
    int noOfVertices;
    LinkedList<Integer> adjList[];

    Graph(int v) {
        this.noOfVertices = v;
        adjList = new LinkedList[v];
        for(int i = 0; i<v; i++) {
            adjList[i] = new LinkedList<>();
        }
    }
}

public class GraphImpl {

    public void addEdge (Graph g, int src, int dest) {
        g.adjList[src].add(dest);
        g.adjList[dest].add(src);
    }

    public int[][] getAdjMatrix (Graph graph) {
        int v = graph.noOfVertices;
        int[][] matrix = new int [v][v];
        for(int i = 0; i< v; i++) {
            Arrays.fill(matrix[i], 0);
        }
        for(int i = 0; i<v; i++) {
            LinkedList<Integer> list = graph.adjList[i];
            Iterator<Integer> iterator = list.iterator();
            while (iterator.hasNext()) {
                matrix[i][iterator.next()] = 1;
            }
        }
        return matrix;
    }

    public void printGraphAdjMatrix (Graph graph) {
        int[][] matrix = getAdjMatrix(graph);
        for(int i = 0; i<graph.noOfVertices; i++) {
            System.out.println(Arrays.toString(matrix[i]));
        }
    }

    public void printGraphAdjList (Graph graph) {
        for(int i = 0; i<graph.noOfVertices; i++) {
            LinkedList<Integer> list = graph.adjList[i];
            Iterator<Integer> iterator = list.iterator();
            System.out.print(i + "->");
            while (iterator.hasNext()) {
                System.out.print(iterator.next() + "->");
            }
            System.out.println("/");
        }
    }

    public static void main(String[] args) {
        GraphImpl graphImpl = new GraphImpl();
        Graph graph = new Graph(5);
        graphImpl.addEdge(graph, 0,4);
        graphImpl.addEdge(graph, 0,1);
        graphImpl.addEdge(graph, 1,3);
        graphImpl.addEdge(graph, 2,4);
        graphImpl.printGraphAdjList(graph);
        System.out.println();
        graphImpl.printGraphAdjMatrix(graph);
    }
}
