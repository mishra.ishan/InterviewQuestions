package SeptemberChallenge;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

/**
 * Created by ishan.mishra on 9/2/17.
 */
public class ChefSum {
    public static void main(String... args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(br.readLine());
        while(t != 0) {
            int n1 = Integer.parseInt(br.readLine());
            String s = br.readLine();
            String[] split = s.split(" ");
            Long [] a = new Long[n1];
            BigInteger[] prefixSum = new BigInteger[n1];
            BigInteger[] suffixSum = new BigInteger[n1];
            for(int j=0; j<split.length; j++) {
                a[j] = Long.parseLong(split[j]);
                String val = a[j].toString();
                BigInteger big = new BigInteger(val);
                prefixSum[j] = (j==0) ?  big : (prefixSum[j-1].add(big));
            }
            for(int j=n1-1; j >= 0; j--) {
                String val = a[j].toString();
                BigInteger big = new BigInteger(val);
                suffixSum[j] = j == n1-1 ? big : (suffixSum[j+1].add(big));
            }
//            System.out.println(a);
//            System.out.println(prefixSum);
//            System.out.println(suffixSum);
            BigInteger min = new BigInteger(String.valueOf(Integer.MAX_VALUE));
            int min_index = 0;
            for(int i=0; i<n1; i++) {
                BigInteger first = prefixSum[i].add(suffixSum[i]);
                if(!first.equals(min) && first.min(min).equals(first)) {
                    min = first;
                    min_index = i;
                }
            }
            System.out.println(min_index+1);
            t--;
        }
    }
}
