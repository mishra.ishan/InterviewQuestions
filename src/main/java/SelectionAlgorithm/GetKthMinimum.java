package SelectionAlgorithm;

import java.util.Arrays;

public class GetKthMinimum {
    public static void main(String[] args) {
        int[] arr = new int[]{19,65,2345,5567,322,44};
        GetKthMinimum getKthMinimum = new GetKthMinimum();
        System.out.println(getKthMinimum.medianOfMediansAlgorithm(arr, 0, arr.length - 1, 3));
    }

    public int quickSelect (int[] arr, int k, int l, int r) {
        if(l==r) {
            return arr[l];
        }
        if(l<r && k>0) {
            int index = partition(arr, l, r);
            if (index - l == k - 1) {
                return arr[index];
            }
            if (index - l > k-1) {
                return quickSelect(arr, k, l, index - 1);
            } else {
                /*
                new k = k - (index - l) - 1 = k - index + l - 1
                index - l gives the no of elements that we are discarding this time. We are not discarding l elements, neither we are discarding index elements.
                -1 in the end is cause we are also not taking the indexth term (inclusive subtraction)
                 */
                return quickSelect(arr, k - index + l - 1, index + 1, r);
            }
        }
        return Integer.MIN_VALUE;
    }

    private int partition(int[] arr, int l, int r) {
        int val = arr[r];
        int i = l-1;
        for(int j=l; j<r; j++) {
            if(arr[j] < val) {
                swap(arr,++i,j);
            }
        }
        swap(arr, ++i, r);
        return i;
    }

    private void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    private int findMedian(int[] arr, int start, int noOfelement) {
        Arrays.sort(arr, start, start + noOfelement - 1);
        return arr[(start + start + noOfelement - 1) / 2];
    }

    public int medianOfMediansAlgorithm(int[] arr, int l, int r, int k) {
        if (k > 0 && k <= r - l + 1) {
            int n = r - l + 1;
            int[] medians = new int[(n)/5 + 1];
            int i = 0;
            for(; i < n/5; i++) {
                medians[i] = findMedian(arr, l + i*5, 5);
            }
            if(i*5 < n) {
                medians [i] = findMedian(arr, l+i*5, n%5);
            }
            int medianOfMedians = (i == 1) ? medians[i-1] : medianOfMediansAlgorithm(medians, 0, i-1, i/2);
            int indx = medianPartition(arr, l, r, medianOfMedians);
            if(indx - 1 == k - 1) {
                return arr[indx];
            }
            if(indx - 1 > k - 1) {
                return medianOfMediansAlgorithm(arr, l, indx - 1, k);
            }
            return medianOfMediansAlgorithm(arr, indx + 1, r, k - indx + 1 - 1);
        }
        return Integer.MIN_VALUE;
    }

    private int medianPartition(int[] arr, int l, int r, int key) {
        int i;
        for(i=l; i<r; i++) {
            if(arr[i] == key) {
                break;
            }
        }
        swap(arr, i, r);
        int val = arr[r];
        i = l-1;
        for(int k = l; k<=r; k++) {
            if(arr[k] < val) {
                swap(arr, k, ++i);
            }
        }
        swap(arr, r, ++i);
        return i;
    }
}
