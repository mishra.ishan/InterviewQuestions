package GreedyAlgorithm;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;

class Interval {
    int start, end;

    public Interval() {
        this.start = 0;
        this.end = 0;
    }

    public Interval(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public String toString() {
        return start + "->" + end;
    }
}

public class MergeOverlappingIntervals {
    public List<Interval> merge(List<Interval> intervals) {
        if(!intervals.isEmpty()) {
            intervals.sort(new Comparator<Interval>() {
                @Override
                public int compare(Interval interval1, Interval interval2) {
                    if (interval1.end < interval2.end) {
                        return -1;
                    }
                    if (interval1.end == interval2.end) {
                        if (interval1.start < interval2.end) {
                            return -1;
                        }
                        return 1;
                    } else
                        return 1;
                }
            });
            int n = intervals.size();
//        System.out.println("Intervals after sort: " + intervals);
            Stack<Interval> stack = new Stack<>();
            stack.push(intervals.get(0));
            int i = 1;
            while (i < intervals.size()) {
                while (stack.peek().end >= intervals.get(i).start && stack.peek().end <= intervals.get(i).end) {
                    Interval first = stack.peek();
                    Interval second = intervals.get(i);
                    stack.pop();
                    int newStart = first.start < second.start ? first.start : second.start;
                    int newEnd = first.end < second.end ? second.end : first.end;
                    stack.push(new Interval(newStart, newEnd));
                }
                stack.push(intervals.get(i));
                i++;
            }
            intervals.clear();
            while (!stack.isEmpty()) {
                intervals.add(stack.pop());
            }
            return intervals;
        }
        return intervals;
    }

    public static void main(String[] args) {
        Interval interval1 = new Interval(2,3);
        Interval interval2 = new Interval(4,5);
        Interval interval3 = new Interval(5,6);
        Interval interval4 = new Interval(6,7);
        Interval interval5 = new Interval(8,9);
        Interval interval6 = new Interval(1,10);
        List<Interval> intervalsList = new ArrayList<>();
        intervalsList.add(interval1);
        intervalsList.add(interval2);
        intervalsList.add(interval3);
        intervalsList.add(interval4);
        intervalsList.add(interval5);
        intervalsList.add(interval6);
        System.out.println("Merged Intervals: "+new MergeOverlappingIntervals().merge(intervalsList));
    }
}
