package OCPJP;

public class Tester {
    public static void main(String[] args) {
        int i = 10;
        overloadWidenVsAutobox(i);//note it is calling fn(long)(overloader parameter widening) and not fn(Integer)(autoboxing)
    }
    public static void fn(Integer i) {
        System.out.println("Integer: "+i);
    }
    public static void overloadWidenVsAutobox(long l) {
        System.out.println("long: "+l);
    }
}
