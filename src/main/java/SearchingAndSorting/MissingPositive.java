package SearchingAndSorting;

public class MissingPositive {
    public static void main(String... args) {
        MissingPositive missingPositive = new MissingPositive();
        int[] arr = new int[]{1};
        System.out.println(missingPositive.firstMissingPositive(arr));
    }

    private int firstMissingPositive(int[] nums) {
        int n = nums.length;
        int i=0;
        while(i<n) {
            if(nums[i] == i+1) {
                i++;
            }
            else if(nums[i] > 0 && nums[i] <= n && nums[nums[i]-1] != nums[i]) {
                swap(nums, i, nums[i]-1);
            }
            else {
                i++;
            }
        }
        for(int j=0; j<n; j++) {
            if(nums[j] != j+1) {
                return j+1;
            }
        }
        return n+1;
    }
    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}
