package SearchingAndSorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ishan.mishra on 8/31/17.
 */
public class SortUtility {
    public static void main(String... a) {
        int[] arr = new int[]{2,8,7,1,3,5,6,4};
        SortUtility sortUtility = new SortUtility();
//        int[] mergeSort = sortUtility.mergeSort(arr);
//        int[] countSort = sortUtility.countSort(arr, 10);
//        int[] radixSort = sortUtility.radixSort(arr);
//        int[] insertSort = sortUtility.insertionSort(arr);
//        int[] bucketSort = sortUtility.bucketSort(arr);
        int[] quickSort = sortUtility.quickSort(arr);
        System.out.println(Arrays.toString(quickSort));
    }


// ------------------------------------------------ MERGE SORT -------------------------------------------------------
    public int[] mergeSort(int[] arr) {
        int[] helper = new int[arr.length];
        mergeSortHelper(arr, helper, 0, arr.length-1);
        return arr;
    }
    private void mergeSortHelper(int[] arr, int[] helper, int left, int right) {
        if(left < right) {
            int mid = left + (right - left) / 2;    //for 0-14, suppose mid of 4 and 10.
            mergeSortHelper(arr, helper, left, mid);
            mergeSortHelper(arr, helper, mid + 1, right);
            merge(arr, helper, left, mid, right);
        }
    }

    private void merge(int[] arr, int[] helper, int left, int mid, int right) {
//        System.out.println(" Array Received For Merge: " + Arrays.toString(arr) + "(" + left+", "+mid+", "+right+")");
        int current_index = left;
        int left_helper_index = left, right_helper_index = mid+1;
        for(int i=left; i<=right; i++) {
            helper[i] = arr[i];
        }
        while(left_helper_index <= mid && right_helper_index <= right) {
            if(helper[left_helper_index] < helper[right_helper_index]) {
                arr[current_index++] = helper[left_helper_index++];
            }
            else {
                arr[current_index++] = helper[right_helper_index++];
            }
        }
        while(left_helper_index < mid+1) {
            arr[current_index++] = helper[left_helper_index++];
        }
//        System.out.println(" After Merge: " + Arrays.toString(arr));
    }


// ---------------------------------------------- QUICK SORT ---------------------------------------------------
    public int[] quickSort(int[] arr) {
        return quickSortHelper(arr, 0, arr.length-1);
    }

    private int[] quickSortHelper(int[] arr, int low, int high) {
//        System.out.println("< ------------------------------ Low: " + low + " high: " + high);
        if(low < high) {
            int index = partition (arr, low, high);
//            System.out.println("Array after partition: "+Arrays.toString(arr) + " with index: " + index);
            quickSortHelper(arr, low, index-1);
            quickSortHelper(arr, index+1, high);
        }
//        System.out.println("Returned arr: "+Arrays.toString(arr) + " ------------------------------------------ >");
        return arr;
    }

    private int partition(int[] arr, int low, int high) {
        int val = arr[high];
        int i = low - 1, j = low;
        while(j < high) {
            if(arr[j] < val) {      //not equal to maintain stability
                i++;
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
            j++;
        }
        int temp = arr[i+1];
        arr[i+1] = arr[j];
        arr[j] = temp;
        return i+1;
    }

// ---------------------------------------------- SELECTION SORT -------------------------------------------------
    public void selectionSort(int[] arr) {
        int n = arr.length;
        for(int i=0; i<n-1; i++) {
            int min = arr[i], min_index = i;
            for(int j=i; j<n; j++) {
                if(arr[j] < min) {
                    min = arr[j];
                    min_index = j;
                }
            }
            int temp = arr[i];
            arr[i] = arr[min_index];
            arr[min_index] = temp;
        }
    }
//    public int[] bubbleSort(int[] arr) {
//
//    }
    public int[] heapSort(int[] arr) {
        return null;
    }


// ------------------------------------------------- BUCKET SORT -------------------------------------------------
    public int[] bucketSort(int[] arr) {
        int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE, n = arr.length;
        for(int i=0; i<n; i++) {
            if (arr[i] < min) {
                min = arr[i];
            }
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        int noOfBuckets = n;
//        while(range > 0) {
//            range/=10;
//            noOfBuckets*=10;
//        }
        int divider = (((max - min) + 1) / noOfBuckets) + 1;  //divider as per the range of elements
        List<Integer>[] buckets = new ArrayList[noOfBuckets];
        for(int i=0; i<noOfBuckets; i++) {
            buckets[i] = new ArrayList();
        }
        for(int i=0; i<n; i++) {
            buckets[(arr[i]-min)/divider].add(arr[i]);
        }
        int[] ans = new int[n];
        int current = 0;
        for(int i=0; i<noOfBuckets; i++) {
            int[] temp = new int[buckets[i].size()];
            for(int j=0; j<temp.length; j++) {
                temp[j] = buckets[i].get(j);
            }
            temp = insertionSort(temp);
            for(int j=0; j<temp.length; j++) {
                ans[current++] = temp[j];
            }
        }
        return ans;
    }

// ------------------------------------------------------ INSERTION SORT ------------------------------------------
    public int[] insertionSort(int[] arr) {
        int n = arr.length;
        for(int i=1; i<n; i++) {
            int val = arr[i];
            int j = i-1;
            while(j>=0 && val < arr[j]) {
                arr[j+1] = arr[j];
                j--;
                arr[j+1] = val;
            }
        }
        return arr;
    }

//------------------------------------------------------- RADIX SORT -----------------------------------------------
    public int[] radixSort(int[] arr) {
        int maxDigit = 0, n = arr.length;
        for(int i=0; i<n; i++) {
            int tempDigit = 0, tempVal = arr[i];
//            System.out.println("TempVal: "+tempVal);
            while(tempVal > 0) {
                tempVal /= 10;
                tempDigit ++;
//                System.out.println("TempDigit: "+tempDigit);
            }
            if(tempDigit > maxDigit) {
//                System.out.println("maxDigit: "+tempDigit);
                maxDigit = tempDigit;
            }
        }
//        System.out.println("Max digit: "+maxDigit);
        for(int i=0; i<maxDigit; i++) {
            arr = countSortRadixHelper(arr, i);
//            System.out.println(" After iteration: " + i + " Array: " + Arrays.toString(arr));
        }
        return arr;
    }
    private int[] countSortRadixHelper(int[] originalArray, int exp) {
        int n = originalArray.length;
        int ten = 1;
        while(exp > 0) {
            ten = ten * 10;
            exp--;
        }
//        System.out.println("Ten: "+ten);
        int[] b = new int[n];
        int[] c = new int[10];
        for(int i=0; i<n; i++) {
            c[(originalArray[i]/ten)%10] += 1;
        }
//        System.out.println("Array c before cumulative add: "+Arrays.toString(c));
        for(int i=1; i<10; i++) {
            c[i] += c[i-1];
        }
//        System.out.println("Array c after cumulative add: "+Arrays.toString(c));
        for(int i=n-1; i>=0; i--) {
            b[c[(originalArray[i]/ten)%10]-1] = originalArray[i];
            c[(originalArray[i]/ten)%10]--;
        }
        return b;
    }


// ------------------------------------------------------- COUNT SORT -----------------------------------------------
    public int[] countSort(int[] originalArray, int range) {
        int n = originalArray.length;
        int[] finalArray = new int[n];
        int k = range; //assuming the range is from 0 - range
        int[] frequencyArray = new int[k];
        for(int i=0; i<n; i++) {
            frequencyArray[originalArray[i]] += 1;
        }
        for(int i=1; i<k; i++) {
            frequencyArray[i] += frequencyArray[i-1];
        }
        for(int i=n-1; i>=0; i--) {
            finalArray[frequencyArray[originalArray[i]]-1] = frequencyArray[originalArray[i]];
            frequencyArray[originalArray[i]]--;
        }
        return finalArray;
    }
}
