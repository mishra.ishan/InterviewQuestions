package SearchingAndSorting;

import java.util.Arrays;

public class PartitionOddAndEven {
    public static void main(String[] args) {
        int[] arr = new int[]{1,2,3,4,5,6,7};
        PartitionOddAndEven partition = new PartitionOddAndEven();
        partition.partition(arr);
        System.out.println("After Partition: "+ Arrays.toString(arr));
    }

    public void partition(int[] arr) {
        int n = arr.length;
        int pivot = n-1;
        int pivotElement = arr[pivot];
        for(int i=-1, j=0; j<pivot; j++) {
            if(arr[j]%2==0) {
                swap(arr,++i, j);
            }
        }
    }

    private void swap (int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
