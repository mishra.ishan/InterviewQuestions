package SearchingAndSorting;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RepeatedDoubling {
    public static void main(String[] args) throws IOException{
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String s = bufferedReader.readLine();
        List<Integer> list = new ArrayList<>();
        String[] split = s.split(",");
        for(String str : split) {
            list.add(Integer.parseInt(str));
        }
        Integer[] intArray = list.toArray(new Integer[list.size()]);
        Arrays.sort(intArray);
        int b = Integer.parseInt(bufferedReader.readLine());
        while(Arrays.binarySearch(intArray, b) >= 0 ) {
            b = b * 2;
        }
        System.out.println("The element: "+b);
    }
}
