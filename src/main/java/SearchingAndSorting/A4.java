package SearchingAndSorting;

import java.util.Arrays;

public class A4 {
    public static void main(String[] args) {
        int[] a = new int[]{12,489,7,79,6,77,41};
        int[] b = new int[]{96,74,7};
        System.out.println(Arrays.toString(new A4().countSmallOrEqual(a, b)));
    }

    public int[] countSmallOrEqual(int[] a, int[] b) {
        int n1 = a.length, n2 = b.length;
        int[] ans = new int[n2];
        Arrays.sort(a);
        for(int i=0; i<n2; i++) {
            int val = b[i];
            int indx = Arrays.binarySearch(a, val);
            if(indx >= 0) {
                ans[i] = indx + 1;
            }
            else {
                ans[i] = Math.abs(indx) - 1;
            }
        }
        return ans;
    }
}
