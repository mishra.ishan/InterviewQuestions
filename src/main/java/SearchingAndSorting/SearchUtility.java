package SearchingAndSorting;

import java.util.Arrays;

public class SearchUtility {
    public static void main(String[] args) {
        int[] arr = new int[]{9,3,67,8,0};
        SearchUtility searchUtility = new SearchUtility();
        SortUtility sortUtility = new SortUtility();
        arr = searchUtility.testMergeSort(arr);
        System.out.println("Array after sorting: " + Arrays.toString(arr));
        int val = 67;
        int pos = searchUtility.binarySearch(arr, val);
        System.out.println("Position of element: "+pos);
    }

    private int[] testMergeSort(int[] arr) {
        int[] helper = new int[arr.length];
        mergeSortHelper(arr, 0, arr.length-1, helper);
        return arr;
    }

    private void mergeSortHelper(int[] arr, int low, int high, int[] helper) {
        if(low < high) {
            int mid = (low+high)/2;
            mergeSortHelper(arr, low, mid, helper);
            mergeSortHelper(arr, mid+1, high, helper);
            merge(arr, low, mid, high, helper);
        }
    }
    private int[] merge (int[] arr, int low, int mid, int high, int[] helper) {
        int n = arr.length;
//        System.out.println("<------------------ Array received: "+Arrays.toString(arr)+" low:"+low+" mid:"+mid+" high:"+high);
        System.arraycopy(arr, low, helper, low, high-low+1);
//        System.out.println("After Array copy: "+Arrays.toString(helper));
        int current = low, leftHelper = low, rightHelper = mid+1;
        while(leftHelper <= mid && rightHelper <= high) {
            if(helper[leftHelper] < helper[rightHelper]) {
                arr[current++] = helper[leftHelper++];
            }
            else {
                arr[current++] = helper[rightHelper++];
            }
        }
        while(leftHelper <= mid) {
            arr[current++] = helper[leftHelper++];
        }
//        System.out.println("Array returned: "+Arrays.toString(arr)+"------------------------------------ >");
        return arr;
    }

    // ---------------------------------------- BINARY SEARCH --------------------------------
    public int binarySearch(int[] arr, int val) {
        return binarySearchHelper(arr, 0, arr.length-1, val);
    }

    private int binarySearchHelper(int[] arr, int low, int high, int val) {
        if(low <= high) {
            int mid = (low + high) / 2;
            if(arr[mid] == val) {
                return mid;
            }
            else if(val < arr[mid]) {
                return binarySearchHelper(arr, low, mid-1, val);
            }
            else {
                return binarySearchHelper(arr, mid+1, high, val);
            }
        }
        return -1;
    }

}
