package Trees.BinaryTrees;

import java.util.ArrayList;
import java.util.List;

public class TestTree {
    public static void main(String[] args) {
        TreeNode node = new TreeNode(1);
        node.left = new TreeNode(2);
        node.right = new TreeNode(3);
//        node.left.left = new TreeNode(4);
//        node.left.right = new TreeNode(5);
//        node.right.left = new TreeNode(6);
//        node.right.right = new TreeNode(7);
        BinaryTreeUtility binaryTreeUtility = new BinaryTreeUtility();
        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> arr = new ArrayList<>();
        binaryTreeUtility.allRootToLeafPaths(node, ans, arr, 0);
        System.out.println("All root to leaf paths: "+ans);
    }
}
