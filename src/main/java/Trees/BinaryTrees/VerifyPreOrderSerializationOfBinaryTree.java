package Trees.BinaryTrees;

public class VerifyPreOrderSerializationOfBinaryTree {

    public static void main (String[] args) {
        VerifyPreOrderSerializationOfBinaryTree verify = new VerifyPreOrderSerializationOfBinaryTree();
        String s = "9,3,4,#,#,1,#,#,2,#,6,#,#";
        System.out.println(verify.isValidSerialization(s));
    }

    public boolean isValidSerialization(String preorder) {
        if(preorder == null || preorder.length() == 0) {
            return true;
        }
        if(preorder.charAt(0) == '#' && preorder.length() > 1) {
            return false;
        }
        String[] split = preorder.split(",");
        int nonLeaf = 0, leaf = 0, i = 0;
        for(i = 0; i < split.length && leaf != nonLeaf + 1; i++) {
            String s = split[i];
            if(s.equals("#")) {
                leaf++;
            }
            else {
                nonLeaf++;
            }
        }
        return (i == split.length  && nonLeaf + 1 == leaf);
    }
}
