package Trees.BinaryTrees;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BinaryTreeUtility {

    public void printTreeInOrder(TreeNode root) {
        if(root != null) {
            printTreeInOrder(root.left);
            System.out.print(root.val+" ");
            printTreeInOrder(root.right);
        }
    }

    public void printTreePreOrder(TreeNode root) {
        if(root != null) {
            System.out.print(root.val+" ");
            printTreePreOrder(root.left);
            printTreePreOrder(root.right);
        }
    }

    public void printTreePostOrder(TreeNode root) {
        if(root != null) {
            printTreePostOrder(root.left);
            printTreePostOrder(root.right);
            System.out.print(root.val+" ");
        }
    }

    public void printTreeLevelOrder(TreeNode root) {
        if(root == null) {
            return;
        }
        Queue<TreeNode> helperQueue = new LinkedList<>();
        helperQueue.add(root);
        helperQueue.add(null);
        int level = 0;
        System.out.print("Level: "+level+" --> ");
        while(true) {
            TreeNode front = helperQueue.poll();
            if(front != null) {
                System.out.print(front.val+" ");
                if(front.left != null) {
                    helperQueue.add(front.left);
                }
                if(front.right != null) { helperQueue.add(front.right);
                }
            }
            else  {
                if(helperQueue.peek() == null) {
                    break;
                }
                System.out.println();
                helperQueue.add(null);
                System.out.print("Level: "+(++level) + " --> ");
            }
        }
    }

    public int heightOfTree(TreeNode root) {
        if(root == null) {
            return 0;
        }
        return Math.max(heightOfTree(root.left),heightOfTree(root.right))+1;
    }

    public int sizeOfTree(TreeNode root) {
        if(root == null) {
            return 0;
        }
        return sizeOfTree(root.left) + sizeOfTree(root.right) + 1;
    }

    public boolean structurallyIdentical (TreeNode root1, TreeNode root2) {
        if(root1 == null && root2 == null) {
            return true;
        }
        if((root1 == null && root2 != null) || (root1 != null && root2 == null)) {
            return false;
        }
        return structurallyIdentical(root1.left,root2.left) &&
                structurallyIdentical(root1.right, root2.right);
    }

    public TreeNode createMirror(TreeNode root) {
        if(root == null) {
            return null;
        }
        TreeNode rootLeft = root.left;
        TreeNode rootRight = root.right;
        root.left = createMirror(rootRight);
        root.right = createMirror(rootLeft);
        return root;
    }

    public boolean isMirror(TreeNode root1, TreeNode root2) {
        if(root1 == null && root2 == null) {
            return true;
        }
        else if ((root1 == null && root2 != null) || (root1 != null && root2 == null)) {
            return false;
        }
        else if(root1.val != root2.val) {
            return false;
        }
        else return (isMirror(root1.left,root2.right) && isMirror(root1.right,root2.left));
    }

    public int diameterTree(TreeNode root) {
        if(root == null) {
            return 0;
        }
        int leftHeight = heightOfTree(root.left);
        int rightHeight = heightOfTree(root.right);
        int leftDiameter = diameterTree(root.left);
        int rightDiameter = diameterTree(root.right);
        return Math.max(leftHeight+rightHeight+1, Math.max(leftDiameter,rightDiameter));
    }

    public void allRootToLeafPaths(TreeNode root, List<List<Integer>> ans, List<Integer> arr, int index) {
        if(root.left == null && root.right == null) {
            arr.add(root.val);
            System.out.println("Root to leaf path found: "+arr);
            List<Integer> temp = new ArrayList<>();
            for(int i=0; i<arr.size(); i++) {
                temp.add(arr.get(i));
            }
            ans.add(temp);
            arr.remove(index);
            return;
        }
        arr.add(root.val);
        allRootToLeafPaths(root.left, ans, arr, index+1);
        allRootToLeafPaths(root.right, ans, arr, index+1);
        arr.remove(index);
    }

    public TreeNode leastCommonAncestor(TreeNode root, int val1, int val2) {
        if(root == null) {
            return null;
        }
        if(root.val == val1 || root.val == val2) {
            return root;
        }
        TreeNode left = leastCommonAncestor(root.left, val1, val2);
        TreeNode right = leastCommonAncestor(root.right, val1, val2);
        if(left != null && right != null) {
            System.out.println("LCA: "+root.val);
            return root;
        }
        if(left != null) {
            return left;
        }
        return right;
    }
}
