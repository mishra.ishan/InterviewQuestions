package Trees.BinarySearchTrees;


import java.util.ArrayList;
import java.util.List;

public class BinarySearchTreeUtility {

    public void insertVal(TreeNode root, int val) {
        if(root == null) {
            return;
        }
        if(val < root.val) {
            if(root.left == null) {
                root.left = new TreeNode(val);
            }
            else {
                insertVal(root.left, val);
            }
        }
        else {
            if(root.right == null) {
                root.right = new TreeNode(val);
            }
            else {
                insertVal(root.right, val);
            }
        }
    }

    public void printInOrder(TreeNode node) {
        if(node == null) {
            return;
        }
        printInOrder(node.left);
        System.out.print(node.val+" ");
        printInOrder(node.right);
    }

    public boolean isValPresent(TreeNode root, int val) {
        if(root == null) {
            return false;
        }
        if(val == root.val) {
            return true;
        }
        if(val < root.val) {
            return isValPresent(root.left, val);
        }
        else {
            return isValPresent(root.right, val);
        }
    }

    public TreeNode getMin(TreeNode root) {
        if(root.left == null) {
            return root;
        }
        return getMin(root.left);
    }

    public TreeNode getMax(TreeNode root) {
        if(root.right == null) {
            return root;
        }
        return getMax(root.right);
    }

    public TreeNode deleteElement(TreeNode root, int val) {
        if(root == null || (root.left == null && root.right==null && root.val == val)) {
            return null;
        }
        int rootVal = root.val;
        if(val < rootVal) {
            root.left = deleteElement(root.left, val);
        }
        else if(val > rootVal) {
            root.right = deleteElement(root.right, val);
        }
        else {
            if(root.left == null && root.right == null) {
                return null;
            }
            if((root.left != null && root.right == null) ||
                    (root.left == null && root.right != null)) {
                if(root.left != null) {
                    return root.left;
                }
                else {
                    return root.right;
                }
            }
            TreeNode getMax = getMax(root.left);
            root.val = getMax.val;
            getMax.val = rootVal;
            return deleteElement(getMax, val);
        }
        return root;
    }

    public int minAbsoluteDifference (TreeNode root) {
        if(root == null) {
            return Integer.MAX_VALUE;
        }
        List<Integer> minValue = new ArrayList<>();
        minValue.add(Integer.MAX_VALUE);
        List<TreeNode> prev = new ArrayList<>();
        minDiffHelper(root, minValue, prev);
        return minValue.get(0);
    }

    private void minDiffHelper(TreeNode root, List<Integer> minValueTillNow, List<TreeNode> prev) {
        if(root == null) {
            return;
        }
        minDiffHelper(root.left, minValueTillNow, prev);
        if(!prev.isEmpty()) {
            int minVal = minValueTillNow.get(0);
            TreeNode pre = prev.get(0);
            minVal = Math.min(root.val - pre.val, minVal);
            minValueTillNow.remove(0);
            minValueTillNow.add(minVal);
            prev.remove(0);
        }
        prev.add(root);
        minDiffHelper(root.right, minValueTillNow, prev);
    }

    public void printInRange(TreeNode root, int k1, int k2) {
        if(root == null) {
            return;
        }
        if(root.val > k1 && root.val > k2) {
            printInRange(root.left, k1, k2);
        }
        if(root.val >= k1 && root.val <= k2) {
            System.out.print(root.val+" ");
        }
        if(root.val < k1 && root.val < k2) {
            printInRange(root.right, k1, k2);
        }
    }

    public TreeNode convertArrayToBST(int[] arr) {
        return convertToBST(arr,0,arr.length-1);
    }

    private TreeNode convertToBST(int[] arr, int l, int r) {
        if(l > r) {
            return null;
        }
        if(l == r) {
            return new TreeNode(arr[l]);
        }
        int mid = (l+r)/2;
        TreeNode root = new TreeNode(arr[mid]);
        root.left = convertToBST(arr, l, mid-1);
        root.right = convertToBST(arr, mid+1, r);
        return root;
    }
}
