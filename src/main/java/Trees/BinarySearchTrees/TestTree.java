package Trees.BinarySearchTrees;

public class TestTree {
    public static void main(String[] args) {
        TreeNode node = new TreeNode(5);
        node.left = new TreeNode(4);
        node.right = new TreeNode(7);
        BinarySearchTreeUtility binarySearchTreeUtility = new BinarySearchTreeUtility();
//        System.out.println("Current tree: ");
//        binarySearchTreeUtility.printInOrder(node);
//        binarySearchTreeUtility.insertVal(node, -3);
//        binarySearchTreeUtility.insertVal(node, -100);
//        binarySearchTreeUtility.insertVal(node, 40);
//        binarySearchTreeUtility.insertVal(node, 66);
//        binarySearchTreeUtility.insertVal(node, 5);
//        System.out.println("After insertions, BST: ");
//        binarySearchTreeUtility.printInOrder(node);
//        binarySearchTreeUtility.deleteElement(node, 1);
//        System.out.println("After deletion, BST: ");
//        binarySearchTreeUtility.printInOrder(node);
        int[] arr = new int[]{1,2,3,4};
        System.out.println("Array to BST: "+binarySearchTreeUtility.convertArrayToBST(arr));
    }
}
