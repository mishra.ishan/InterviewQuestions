package Trees.BinarySearchTrees;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//https://leetcode.com/problems/convert-bst-to-greater-tree/description/
public class BSTToGreaterTree {

    public BSTToGreaterTree() {
        this.treeElement = new ArrayList<>();
    }

    public static void main(String[] args) {
        int[] arr = {2,5,13};
        BinarySearchTreeUtility utility = new BinarySearchTreeUtility();
        TreeNode treeNode = utility.convertArrayToBST(arr);
        utility.printInOrder(treeNode);
        BSTToGreaterTree bstToGreaterTree = new BSTToGreaterTree();
        treeNode = bstToGreaterTree.convertBSTToGreaterTree(treeNode);
        System.out.println();
        utility.printInOrder(treeNode);
    }

    private List<Integer> treeElement;
    private int i = 0;

    public TreeNode convertBSTToGreaterTree(TreeNode root) {
        inOrderToArray(root);
        for(int i = treeElement.size()-2; i>=0; i--) {
            treeElement.set(i, treeElement.get(i) + treeElement.get(i+1));
        }
        int[] arr = new int[treeElement.size()];
        for(int i = 0; i < treeElement.size(); i++) {
            arr[i] = treeElement.get(i);
        }
        arrayToInorder(root);
        return root;
    }

    private void inOrderToArray (TreeNode root) {
        if(root == null) {
            return;
        }
        inOrderToArray(root.left);
        treeElement.add(root.val);
        inOrderToArray(root.right);
    }

    private void arrayToInorder (TreeNode root) {
        if (root == null) {
            return;
        }
        arrayToInorder(root.left);
        root.val = treeElement.get(i++);
        arrayToInorder(root.right);
    }
}
