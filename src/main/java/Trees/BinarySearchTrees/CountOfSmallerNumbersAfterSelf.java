package Trees.BinarySearchTrees;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

class BST {
    int val, noOfNodesInLeftSubtree, duplicate;
    BST left, right;

    public BST(int val) {
        this.val = val;
        this.noOfNodesInLeftSubtree = 0;
        this.left = null;
        this.right = null;
        this.duplicate = 0;
    }
}

//https://leetcode.com/problems/count-of-smaller-numbers-after-self/description/
public class CountOfSmallerNumbersAfterSelf {
    public static void main (String[] args) {
        int[] a = {4,10,5,2,6};
        CountOfSmallerNumbersAfterSelf count = new CountOfSmallerNumbersAfterSelf();
        System.out.println(count.countSmaller(a));
    }



    public List<Integer> countSmaller(int[] arr) {
        if(arr == null || arr.length == 0) {
            return new ArrayList<>();
        }
        int n = arr.length;
        Integer[] ans = new Integer[n];
        Arrays.fill(ans, -1);
        if(n == 1) {
            ans[0] = 0;
            return Arrays.asList(ans);
        }
        BST bstRoot = new BST(arr[n-1]);
        ans[n-1] = 0;
        for(int i = n-2; i >= 0; i--) {
            BST temp = bstRoot;
            int count = 0;
            int val = arr[i];
            while(temp != null) {
                if (val == temp.val) {
                    count = count + temp.noOfNodesInLeftSubtree; //cause val is also greater than the elements in the left subtree
                    temp.duplicate = temp.duplicate + 1;
                    ans[i] = count;
                    break;
                } else if (val < temp.val) {
                    temp.noOfNodesInLeftSubtree = temp.noOfNodesInLeftSubtree + 1;
                    //Since we are adding to the left subtree of node, hence increment the number of nodes in left subtree by 1.
                    if(temp.left == null) {
                        temp.left = new BST(val);
                        //When we create a new node, that time, we don't have any element in its left subtree
                        ans[i] = count;
                        break;
                    }
                    else {
                        temp = temp.left;
                    }
                } else {
                    count = count +  temp.noOfNodesInLeftSubtree + temp.duplicate + 1;// greater than the current node, hence adding
                    if(temp.right == null) {
                        temp.right = new BST(val);
                        //When we create a new node, that time, we don't have any element in its left subtree
                        ans[i] = count;
                        break;
                    }
                    else {
                        /*
                        Since taking a right turn:-
                        1. Since current val > node.val, hence current val > all nodes which are left of node. hence count += temp.count
                        2. current node might have duplicate. But since current val > node, hence current val > duplicate of node too. hence count += temp.duplicate
                         */
                        temp = temp.right;
                    }
                }
            }
        }

        return Arrays.asList(ans);
    }

    public List<Integer> countSmallerViaStack(int[] nums) {
        if(nums == null || nums.length == 0) {
            return new ArrayList<>();
        }
        int n = nums.length;
        List<Integer> ans = new ArrayList<>(n);
        Integer[] count = new Integer[n];
        Stack<Integer>[] stacks = new Stack[2];
        stacks[0] = new Stack<>();
        stacks[1] = new Stack<>();
        count[n-1] = 0;
        stacks[0].add(nums[n-1]);
        for(int i = n-2; i >= 0; i--) {
            count[i] = putIntoAnotherStackTillFoundPeekGreater(stacks, nums[i]);
        }
        ans = Arrays.asList(count);
        return ans;
    }

    private int putIntoAnotherStackTillFoundPeekGreater(Stack<Integer>[] stacks, int num) {
        Stack<Integer> mainStack = stacks[0];
        Stack<Integer> auxStack = stacks[1];
        int size = mainStack.size(), count = 0;
        while(!mainStack.isEmpty() && mainStack.peek() >= num) {
            auxStack.add(mainStack.pop());
            count++;
        }
        mainStack.add(num);
        while(!auxStack.isEmpty()) {
            mainStack.add(auxStack.pop());
        }
        return size - count;
    }
}
/*
Basic idea is to keep the number of nodes smaller than the current node always available. Hence, we use Trees.BinarySearchTrees.BST which can store this number in the nodes. The value in the node can be updated while traversing. Hence, at any point, we can tell how many nodes are smaller than node x (since we are keeping the number of nodes in its left subtree in a variable x.noOfNodesFromLeftSubtree).
 */
