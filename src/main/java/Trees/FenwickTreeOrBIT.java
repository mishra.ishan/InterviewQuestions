package Trees;

import java.util.Arrays;

/**
 * https://www.youtube.com/watch?v=CWDQJGaN1gY
 * Helps solve prefix sum of an array queries. For example, if we have an array from 0->n, fenwick tree or Binary
 * Indexed Tree helps us solve ranged queries like what is the sum of array indexes from 0 -> i (0 < i <= n), for example,
 * what is the sum of elements from 0 -> 6 (say).
 *
 * One way to solve it is to store the prefix array sum of the array. For example consider the array:
 * arr = [4, 3, 8, 9, -1, 8]
 *
 * Now, the prefix sum array for the same shall be sum = [4, 7, 15, 24, 23, 31].
 * Note that we can solve the sum range queries in O(1) time. For example, sum of elements from indx 2, 4 shall be
 * sum[4] - sum[2]. It is because sum[4] = sum till 4th indx and sum[2] = sum till 2nd indx. Hence, sum[4] - sum[2] =
 * Sum from 2nd indx till 4th indx.
 * But update queries in this array would require O(n) time. It is because we would have to re-create the prefix sum
 * array. Thus, this solution is the best if we have minimum or no updates in the array, but it is really bad if we
 * have a lot of updates in our array.
 *
 * Another solution is segment trees. The problem with segment trees is that it is more complicated than
 * the BIT (Binary Indexed tree). It takes in worst case 4(n) space, also in worst case it takes 4(log(n)) time to do
 * a ranged sum query.
 *
 * Thus, BIT is a good choice for these queries. The space it takes to store fenwick tree is O(n). Time to search
 * into fenwick tree (for ranged sum queries) is O(log(n)). Amount of time to update an element in fenwick tree is O
 * (log(n)). Amount of time to create fenwick tree for the first time is nlog(n). Thus, final params for fenwick tree
 * are:-
 *
 * CREATION TIME: nlog(n)
 * RANGED SUM TIME: log(n)
 * ELEMENT UPDATE TIME: log(n).
 * SPACE: O(n).
 */
public class FenwickTreeOrBIT { //Main operations to be used: GP and GN. Remeber that it is a good parent bad son deal. Works on 2's compliments.

    /**
     *  Implementation details of the fenwick tree. It requires a LOT OF BINARY CALCULATIONS
     *  Two methods we shall be using. First is getParent (in order to do a ranged sum query) and Second is getNext
     *  (in order to build the tree and perform updates to it)
     *
     *  Get Parent (GP) of a particular index (say indx 7) :-
     *  1. Get 2's compliment of that index (eg, 7 = 111. 2's compliment of 7 = 001). (2)
     *  2. And it with original number (eg 001 & 111 = 001). (&)(A)
     *  3. Subtract the obtained digit from original number (111 - 001 = 6). (-)(S)
     *  Short form is 2AS. Parent has 2 children. Hence 2 asses to support. Parent is good.
     *
     *  Get Next (GN) of a particular index (say indx 1):-
     *  1. Get 2's compliment of that index + 1 (eg, to find gn of 1, we take 2's compliment of 2 = 0010. 2' compliment of 2 = 1110). (2)
     *  2. And the obtained with the original number + 1 (eg, 1110 & 0010 = 0010 = 2). (A)
     *  3. Add to the original number + 1 (eg, 0010 + 0010 = 4). (A)
     *  Short form is 2AA. Double asshole (Silicon Valley).
     *
     * Get Prefix sum:- Go to fenwickTree[index+1]. And add all parent nodes.
     * Update an element at index i (in original array):- Make an update at fenwickTree[i+1] and all it's next nodes.
     */

    /**
     * @param originalArray: Array whose fenwick tree is to be created
     * @return Fenwick tree
     */
    private int[] createFenwickTree (int[] originalArray) {
        int[] fenwickTree = new int[originalArray.length + 1];
        Arrays.fill(fenwickTree, 0);
        for(int i = 1; i < fenwickTree.length; i++) {
            //Start updating from index 1 till the last index in fenwick tree.
            updateFenwickTree(fenwickTree, originalArray[i-1], i);
        }
        return fenwickTree;
    }

    /**
     * (MAIN) Update query:- Update a particular number (numberToBeAdded) at a particular index (index) in the original array. In this method however, we perform the update on fenwick tree.
     *
     * @param fenwickTree
     * @param numberToBeAdded: Individual number which is to be updated at a particular index (update query)
     * @param index: index to be updated
     */
    private void updateFenwickTree(int[] fenwickTree, int numberToBeAdded, int index) {
        while(index < fenwickTree.length) {
            fenwickTree[index] += numberToBeAdded;
            index = getNextIndex(index);
        }
    }

    /**
     * (MAIN) Ranged Sum Query:- Get sum till a particular index
     *
     * @param fenwickTree
     * @param index
     * @return sum till a particular index.
     */
    private int getSum (int[] fenwickTree, int index) {
        index++;
        int sum = 0;
        while( index > 0) {
            sum += fenwickTree[index];
            index = getParent(index);
        }
        return sum;
    }

    /**
     *
     * Get the next index where the update is to be made
     * Method to do that:- 2AA (a child like double asshole)
     * 1. iRes = 2's compliment (indx).
     * 2. res = iRes & indx.
     * 3. next = res + indx
     *
     * @param index
     * @return the next index where the update is to be made.
     */
    private int getNextIndex(int index) {
        int twoCompliment = ~index + 1;
        int intermediateResult = twoCompliment & index;
        return index + intermediateResult;
    }

    /**
     *
     * Get the parent node index for the given index.
     * Method:- 2AS
     * 1. 2's compliment.
     * 2. And with original number.
     * 3. Subtract with original number.
     *
     * @param index
     * @return parent node index.
     */
    private int getParent(int index) {
        int twoCompliment = ~index + 1;
        int intermediateResult = twoCompliment & index;
        return index - intermediateResult;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{4, 1, 6, -2, 7, 6};
        FenwickTreeOrBIT bit = new FenwickTreeOrBIT();
        int[] fenwickTree = bit.createFenwickTree(arr);
        System.out.println(bit.getSum(fenwickTree, 0));
        System.out.println(bit.getSum(fenwickTree, 1));
        System.out.println(bit.getSum(fenwickTree, 2));
        System.out.println(bit.getSum(fenwickTree, 3));
        System.out.println(bit.getSum(fenwickTree, 4));
        System.out.println(bit.getSum(fenwickTree, 5));
    }
}
