package HeapsAndPriorityQueue;

public class TestHeap {
    public static void main(String[] args) {
        MedianHeapImpl medianHeap = new MedianHeapImpl(100);
        medianHeap.insert(1);
        System.out.println("Median: "+medianHeap.getMedian());
        medianHeap.insert(9);
        System.out.println("Median: "+medianHeap.getMedian());
        medianHeap.insert(0);
        System.out.println("Median: "+medianHeap.getMedian());
        medianHeap.insert(7);
        System.out.println("Median: "+medianHeap.getMedian());
    }
}
