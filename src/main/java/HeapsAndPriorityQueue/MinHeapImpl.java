package HeapsAndPriorityQueue;

import java.util.Arrays;

public class MinHeapImpl {
    int[] arr;
    int capacity;
    int count;

    public MinHeapImpl(int capacity) {
        this.capacity = capacity;
        this.arr = new int[capacity];
        count = 0;
    }

    public MinHeapImpl(int[] arr) {
        this.count = arr.length;
        this.capacity = arr.length;
        this.arr = arr;
        heapify();
    }

    public int getMin() {
        return arr[0];
    }

    public int getParentIndx(int i) {
        if((i-1)/2>=count) {
            return -1;
        }
        return (i-1)/2;
    }

    public int getLeftChildIndx(int i) {
        return (2*i+1 >= count)?-1:(2*i+1);
    }

    public int getRightChildIndx(int i) {
        return (2*i+2 >= count)?-1:(2*i+2);
    }

    private void swapIndexElements (int a_indx, int b_indx) {
        int temp = arr[a_indx];
        arr[a_indx] = arr[b_indx];
        arr[b_indx] = temp;
    }

    public void percolateDown (int i) {
        int leftChild = getLeftChildIndx(i);
        int rightChild = getRightChildIndx(i);
        int maxIndx = i;
        if(leftChild != -1) {
            if(arr[maxIndx] > arr[leftChild]) {
                maxIndx = leftChild;
            }
        }
        if(rightChild != -1) {
            if(arr[maxIndx] > arr[rightChild]) {
                maxIndx = rightChild;
            }
        }
        if(maxIndx != i) {
            swapIndexElements(maxIndx, i);
            percolateDown(maxIndx);
        }
    }

    public void heapify() {
        for(int i = getParentIndx(count-1); i>=0; i--) {
            percolateDown(i);
        }
    }

    public void insertElement(int val) {
        if(count + 1 == capacity) {
            System.out.println("Already at full capacity");
            return;
        }
        arr[count++] = val;
        int i = count - 1;
        while(i>=1 && arr[i] < arr[getParentIndx(i)]) {
            swapIndexElements(i, getParentIndx(i));
            i=getParentIndx(i);
        }
    }

    public void heapSort() {
        int len = count;
        for(int i = 0; i<len; i++) {
            swapIndexElements(0,count-1);
            count--;
            percolateDown(0);
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(arr);
    }
}
