package HeapsAndPriorityQueue;

import java.util.Arrays;

public class StackUsingHeap {

    public static void main(String[] args) {
        StackUsingHeap stack = new StackUsingHeap(10);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        System.out.println("Popped Element: "+stack.pop());
        System.out.println("Top of stack: "+stack.peek());
    }


    int[][] arr;
    int capacity;
    int count;
    int priority = 0;

    public StackUsingHeap(int capacity) {
        this.capacity = capacity;
        this.arr = new int[capacity][2];
    }

    private int getParentIndex(int i) {
        int parentIndx = (i-1)/2;
        if(parentIndx >= count) {
            return -1;
        }
        return parentIndx;
    }

    private int getLeftChildIndx(int i) {
        int left = 2*i+1;
        if(left >= count) {
            return -1;
        }
        return left;
    }

    private int getRightChildIndx(int i) {
        int right = 2*i+2;
        if(right >= count) {
            return -1;
        }
        return right;
    }

    private void swapIndexElements(int i, int j) {
        int[] temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    private void insert(int val, int priority) {
        if(count+1 == capacity) {
            System.out.println("Heap is at full capacity");
            return;
        }
        arr[count][0] = val;
        arr[count++][1] = priority;
        int i=count-1, parentIndx = getParentIndex(count-1);
        while(i>=1 && arr[parentIndx][1] < arr[i][1]) {
            swapIndexElements(i, parentIndx);
            i=getParentIndex(i);
        }
    }

    private void percolateDown(int i) {
        int left = getLeftChildIndx(i);
        int right = getRightChildIndx(i);
        int maxIndx = i;
        if(left != -1) {
            if(arr[maxIndx][1] < arr[left][1]) {
                maxIndx = left;
            }
        }
        if(right != -1) {
            if(arr[maxIndx][1] < arr[right][1]) {
                maxIndx = right;
            }
        }
        if(maxIndx != i) {
            percolateDown(maxIndx);
        }
    }

    private int deleteMaxPriority() {
        if(count == 0) {
            System.out.println("Empty Stack");
            return Integer.MIN_VALUE;
        }
        int ret = arr[0][0];
        swapIndexElements(0, count-1);
        count--;
        percolateDown(0);
        return ret;
    }

    public void push (int val) {
        this.insert(val, priority++);
    }

    public int pop() {
        return deleteMaxPriority();
    }

    public int peek() {
        if(count == 0) {
            System.out.println("Empty Stack");
            return Integer.MIN_VALUE;
        }
        return arr[0][0];
    }
}
