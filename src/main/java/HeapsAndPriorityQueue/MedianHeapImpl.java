package HeapsAndPriorityQueue;


public class MedianHeapImpl {
    private int count;
    private int capacity;
    private MinHeap minHeap;
    private MaxHeap maxHeap;

    public MedianHeapImpl(int capacity) {
        this.capacity = capacity;
        this.count = 0;
        this.minHeap = new MinHeap(capacity/2+1);
        this.maxHeap = new MaxHeap(capacity/2);
    }

    public int getMedian () {
        int maxElement = maxHeap.getMax();
        int minElement = minHeap.getMin();
        if(count%2 == 0) {
            return (maxElement + minElement) / 2;
        }
        else {
            return minElement;
        }
    }

    public void insert(int val) {
        if(val > maxHeap.getMax()) {
            minHeap.insert(val);
        }
        else {
            maxHeap.insert(val);
        }
        int minSize = minHeap.getCount();
        int maxSize = maxHeap.getCount();
        if(minSize - maxSize > 1) {
            int deletedVal = minHeap.deleteMin();
            if(deletedVal != Integer.MIN_VALUE){
                maxHeap.insert(deletedVal);
            }
        }
        if(maxSize > minSize) {
            int deletedVal = maxHeap.deleteMax();
            if(deletedVal != Integer.MIN_VALUE) {
                minHeap.insert(deletedVal);
            }
        }
        count++;
    }
}

class MinHeap {
    private int count;
    private int capacity;
    private int[] arr;

    public int getCount() {
        return count;
    }

    public MinHeap(int capacity) {
        this.capacity = capacity;
        arr = new int[capacity];
    }

    private int getParentIndx(int i) {
        int parentIndx = (i-1)/2;
        return parentIndx;
    }

    private int getLeftChildIndx(int i) {
        int leftChildIndx = 2*i+1;
        if(leftChildIndx >= count) {
            return -1;
        }
        return leftChildIndx;
    }

    private int getRightChildIndx(int i) {
        int rightChildIndx = 2*i+2;
        if(rightChildIndx >= count) {
            return -1;
        }
        return rightChildIndx;
    }

    private void swapIndexedPlaces(int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    private void percolate (int i) {
        int left = getLeftChildIndx(i);
        int right = getRightChildIndx(i);
        int minIndx = i;
        if(left != -1) {
            if(arr[left] < arr[minIndx]) {
                minIndx = left;
            }
        }
        if(right != -1) {
            if(arr[right] < arr[minIndx]) {
                minIndx = right;
            }
        }
        if(minIndx != i) {
            swapIndexedPlaces(i, minIndx);
            percolate(minIndx);
        }
    }

    public void insert (int val) {
        if(count == capacity) {
            System.out.println("Min heap is at capacity");
            return;
        }
        arr[count++] = val;
        int i = count-1;
        while(i >= 1 && arr[i] < arr[getParentIndx(i)]) {
            swapIndexedPlaces(i, getParentIndx(i));
            i = getParentIndx(i);
        }
    }

    public int getMin() {
        if(count == 0) {
            System.out.println("Empty Min Heap");
            return Integer.MIN_VALUE;
        }
        return arr[0];
    }

    public int deleteMin() {
        if(count == 0) {
            System.out.println("Empty Heap");
            return Integer.MIN_VALUE;
        }
        int val = arr[0];
        swapIndexedPlaces(0, count-1);
        count--;
        percolate(0);
        return val;
    }
}

class MaxHeap {
    private int count;
    private int capacity;
    private int[] arr;

    public int getCount() {
        return count;
    }

    public MaxHeap(int capacity) {
        this.capacity = capacity;
        this.arr = new int[capacity];
    }

    private int getParentIndx(int i) {
        int parentIndx = (i-1)/2;
        if(parentIndx < 0) {
            return -1;
        }
        return parentIndx;
    }

    private int getLeftChildIndx(int i) {
        int leftChildIndx = 2*i+1;
        if(leftChildIndx >= count) {
            return -1;
        }
        return leftChildIndx;
    }

    private int getRightChildIndx(int i) {
        int rightChildIndx = 2*i+2;
        if(rightChildIndx >= count) {
            return -1;
        }
        return rightChildIndx;
    }

    private void swapIndexedPlaces(int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    private void percolate (int i) {
        int left = getLeftChildIndx(i);
        int right = getRightChildIndx(i);
        int maxIndx = i;
        if(left != -1) {
            if(arr[left] > arr[maxIndx]) {
                maxIndx = left;
            }
        }
        if(right != -1) {
            if(arr[right] > arr[maxIndx]) {
                maxIndx = right;
            }
        }
        if(maxIndx != i) {
            swapIndexedPlaces(i, maxIndx);
            percolate(maxIndx);
        }
    }

    public void insert (int val) {
        if(count == capacity) {
            System.out.println("Max heap is at capacity");
            return;
        }
        arr[count++] = val;
        int i = count-1;
        while(i >= 1 && arr[i] > arr[getParentIndx(i)]) {
            swapIndexedPlaces(i, getParentIndx(i));
            i = getParentIndx(i);
        }
    }

    public int getMax() {
        if(count == 0) {
            System.out.println("Empty Max heap");
            return Integer.MIN_VALUE;
        }
        return arr[0];
    }

    public int deleteMax() {
        if(count == 0) {
            System.out.println("Empty Heap");
            return Integer.MIN_VALUE;
        }
        int val = arr[0];
        swapIndexedPlaces(0, count-1);
        count--;
        percolate(0);
        return val;
    }
}