package ArraysAndStrings;

import java.util.HashMap;
import java.util.Map;

//https://leetcode.com/problems/longest-palindrome/description/
public class LongestPalindrome {
    public static void main (String[] args) {
        String a = "ab";
        LongestPalindrome longestPalindrome = new LongestPalindrome();
        System.out.println(longestPalindrome.longestPalindrome(a));
    }

    public int longestPalindrome(String s) {
        if(s == null && s.length() == 0) {
            return 0;
        }
        boolean oneReceived = false;
        Map<Character, Integer> map = new HashMap<>();
        for(int i = 0; i<s.length(); i++) {
            char c = s.charAt(i);
            if(map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
            }
            else {
                map.put(c, 1);
            }
        }
        int len = 0;
        for(Map.Entry<Character, Integer> pair : map.entrySet()) {
//            System.out.println("char: " + pair.getKey() + "\t" + " num: " + pair.getValue());
            int num = pair.getValue();
            while (num >= 2) {
                len += 2;
                num -= 2;
            }
            if(num == 1 && !oneReceived) {
                len += num;
                oneReceived = true;
            }
        }
        return len;
    }
}
