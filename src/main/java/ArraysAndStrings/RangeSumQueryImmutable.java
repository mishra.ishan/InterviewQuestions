package ArraysAndStrings;

//https://leetcode.com/problems/range-sum-query-immutable/description/
public class RangeSumQueryImmutable {

    private int[] arr;

    public static void main (String[] args) {
        int[] arr = {-2,0,3,-5,2,-1, -6, 2};
        RangeSumQueryImmutable rangeSumQueryImmutable = new RangeSumQueryImmutable(arr);
        System.out.println(rangeSumQueryImmutable.sumRange(2, 6));
    }

    public int sumRange(int i, int j) {
        if(i == 0) {
            return arr[j];
        }
        if(j == arr.length - 1) {
            return arr[arr.length-1] - arr[i-1];
        }
        return arr[arr.length - 1] - (arr[i-1] + (arr[arr.length-1] - arr[j]));
    }

    public RangeSumQueryImmutable(int[] arr) {
        for(int i = 1; i < arr.length; i++) {
            arr[i] += arr[i-1];
        }
        this.arr = arr;
    }
}
