package ArraysAndStrings;

import java.util.HashMap;
import java.util.Map;

//https://leetcode.com/problems/longest-substring-without-repeating-characters/description/
public class LongestSubstringWithoutRepeatingCharacter {
    public static void main(String[] args) {
        LongestSubstringWithoutRepeatingCharacter lswrc = new LongestSubstringWithoutRepeatingCharacter();
        String a = "pwwkew";
        System.out.println(lswrc.lengthOfLongestSubstring(a));
    }

    public int lengthOfLongestSubstring(String s) {
        if(s == null || s.length() == 0) {
            return 0;
        }
        int maxLen = 0, len = 0;
        for(int i = 0; i < s.length(); i++) {
            Map<Character, Boolean> alreadyOccuredCharacters = new HashMap<>();
            len = 0;
            int j;
            for( j = i; j < s.length(); j++) {
                char c = s.charAt(j);
                if(alreadyOccuredCharacters.containsKey(c)) {
                    break;
                }
                else {
                    len++;
                    alreadyOccuredCharacters.put(c, true);
                }
            }
            if(len > maxLen) {
                maxLen = len;
            }
        }
        if(len > maxLen) {
            maxLen = len;
        }
        return maxLen;
    }
}
