package ArraysAndStrings;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by ishan.mishra on 8/22/17.
 */
public class I1 {

    public static void main(String[] args) {
        String s1 = "fb";
        Boolean isUnique = isUniqueWithoutDS(s1);
        if(isUnique) {
            System.out.println("True");
        }
        else {
            System.out.println("False");
        }
    }

    public static Boolean isUniqueWithDS(String s1) {
        Map<Character, Boolean> dictionary = new HashMap<>();
        Boolean isUnique = true;
        for(int i=0; i<s1.length(); i++) {
            if(dictionary.containsKey(s1.charAt(i))) {
                isUnique = false;
                break;
            }
            dictionary.put(s1.charAt(i), true);
        }
        return isUnique;
    }

    public static String printBinary(int v) {
        String binary = "";

        return  binary;
    }

    public static Boolean isUniqueWithoutDS(String s1) {
        Boolean isUnique = true;
        int checker = 0;
        for(int i=0; i<s1.length(); i++) {
            int val = s1.charAt(i) - 'a';
            System.out.println("-------------------- For character: " + s1.charAt(i));
            System.out.println("Checker: "+checker);
            System.out.println("val: "+val);
            System.out.println("1<<val: "+(1<<val));
            System.out.println("checker & 1<<val: " + (checker & (1<<val)));
            if((checker & (1<<val)) > 0) {
                isUnique = false;
            }
            System.out.println("checker |= 1<<val: " + (checker |= (1<<val)));
            checker |= (1<<val);
        }
        return isUnique;
    }
}
