package ArraysAndStrings;

import java.util.LinkedList;
import java.util.Queue;

public class RemoveRecursiveDuplicates {
    public static void main(String[] args) {
       StringBuffer sb = new StringBuffer("axzzy");
       System.out.println(sb + " " + sb.length());
       sb.deleteCharAt(0);
       System.out.println(sb + " " + sb.length());
        sb.deleteCharAt(sb.length()-1);
        System.out.println(sb + " " + sb.length());

    }

//    public String removeAdjDuplicates(String src, int i, int n, char lastSeen) {
//        if(i+1 == n) {
//            return String.valueOf(src.charAt(i));
//        }
//        String remStr = "";
//        if(src.charAt(i) != src.charAt(i+1)) {
//            remStr = removeAdjDuplicates(src,i+1,n,src.charAt(i));
//        }
//        else {
//            char c = src.charAt(i);
//            while(i+1 != n && src.charAt(i) == src.charAt(i+1)) {
//                i++;
//            }
//            i++;
//            remStr = removeAdjDuplicates(src,i+1,n,c);
//        }
//        if(remStr.charAt(0) == lastSeen) {
//            return remStr.charAt(i+1);
//        }
//    }
}
