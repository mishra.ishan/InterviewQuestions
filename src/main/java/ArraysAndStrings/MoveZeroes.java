package ArraysAndStrings;

import java.util.Arrays;

public class MoveZeroes {

    public static void main (String[] args) {
        MoveZeroes moveZeroes = new MoveZeroes();
        int[] a = {1};
        moveZeroes.moveZeroes(a);
        System.out.println(Arrays.toString(a));
    }

    public void moveZeroes(int[] nums) {
        if(nums == null || nums.length == 0) {
            return;
        }
        int i = 0, j = 0, n = nums.length;
        while(j < nums.length) {
            if(nums[j] != 0) {
                swap(i++, j++, nums);
            }
            else {
                j++;
            }
        }
    }

    private void swap (int i, int j, int[] arr) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
