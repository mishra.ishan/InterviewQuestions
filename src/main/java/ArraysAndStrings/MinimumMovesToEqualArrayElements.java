package ArraysAndStrings;

import java.util.Arrays;

//https://leetcode.com/problems/minimum-moves-to-equal-array-elements-ii/description/
public class MinimumMovesToEqualArrayElements {
    public static void main(String[] args) {
        int[] a = {100,-100, -100};
        MinimumMovesToEqualArrayElements moves = new MinimumMovesToEqualArrayElements();
        System.out.println(moves.minMoves2(a));
    }

    public int minMoves2(int[] nums) {
        if(nums == null || nums.length < 2) {
            return 0;
        }
        Arrays.sort(nums);
        int mid = nums.length/2;
        int normalizer = nums[mid];
        int ans = 0;
        for(int val : nums) {
            ans = ans + (Math.abs(normalizer - val));
        }
        return ans;
    }
}
