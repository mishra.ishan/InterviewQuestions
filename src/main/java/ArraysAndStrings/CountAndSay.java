package ArraysAndStrings;

//https://leetcode.com/problems/count-and-say/description/
public class CountAndSay {

    public static void main(String[] args) {
        CountAndSay countAndSay = new CountAndSay();
        System.out.println(countAndSay.countAndSay(6));
    }

    public String countAndSay(int n) {
        if(n <= 0 ) {
            return "";
        }
        if(n <= 5) {
            switch (n){
                case 1:return "1";
                case 2:return "11";
                case 3: return "21";
                case 4: return "1211";
                case 5: return "111221";
            }
        }
        String[] ans = new String[n+1];
        ans[0] = "";
        ans[1] = "1";
        ans[2] = "11";
        ans[3] = "21";
        ans[4] = "1211";
        ans[5] = "111221";
        for(int i = 6; i <= n; i++) {
            int count = 1, currNo = ans[i-1].charAt(0)-48;
            StringBuffer stringBuffer = new StringBuffer();
            for(int j = 1; j < ans[i-1].length(); j++) {
                int temp = ans[i-1].charAt(j) - 48;
                if(currNo != temp) {
                    stringBuffer.append(count).append(currNo);
                    currNo = temp;
                    count = 1;
                }
                else {
                    count++;
                }
                if(j == ans[i-1].length() - 1) {
                    stringBuffer.append(count).append(currNo);
                }
            }
            ans[i] = stringBuffer.toString();
        }
        return ans[n];
    }
}
