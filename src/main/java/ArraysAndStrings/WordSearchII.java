package ArraysAndStrings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ishan.mishra on 9/1/17.
 */
class MatrixElement {
    Character val;
    int x;
    int y;
    boolean isAdjacent (int x1, int y1) {
        return ((x1 == this.x) && ((y1 - this.y) == 1 || (this.y-y1==1)) ||
                ((y1==this.y) && ((this.x-x1) == 1 || (x1-this.x)==1)));
    }

    public MatrixElement(char val, int x, int y) {
        this.val = val;
        this.x = x;
        this.y = y;
    }
}
public class WordSearchII {
    public static void main(String... args) {

    }

    private Map<Character, List<MatrixElement>> buildMap(char[][] board) {
        Map<Character, List<MatrixElement> > myMap = new HashMap<>();
        for(int i=0; i<board.length; i++) {
            for(int j=0; j<board[i].length; j++) {
                Character c = board[i][j];
                MatrixElement matrixElement = new MatrixElement(c, i,j);
                if(myMap.containsKey(c)) {
                    List<MatrixElement> matrixElements = myMap.get(c);
                    matrixElements.add(matrixElement);
                    myMap.replace(c, matrixElements);
                }
                else {
                    List<MatrixElement> list = new ArrayList<>();
                    list.add(matrixElement);
                    myMap.put(c, list);
                }
            }
        }
        return myMap;
    }
    public List<String> findWords(char[][] board, String[] words) {
        Map<Character, List<MatrixElement>> characterListMap = buildMap(board);
        for(int i=0; i<words.length; i++) {
            char[] word = words[i].toCharArray();
            for(int j=0; j<word.length; j++) {
                Character c = word[j];
                List<MatrixElement> matrixElements = characterListMap.get(c);
                for(MatrixElement m : matrixElements) {

                }
            }
        }
        return null;
    }
}
