package ArraysAndStrings;

//https://leetcode.com/problems/valid-sudoku/description/
public class ValidSudoku {
    public static void main(String[] args) {
        String[][] board = {{".", "8", "7", "6", "5", "4", "3", "2", "1"}, {"2", ".", ".", ".", ".", ".", ".", ".", "."}, {"3", ".", ".", ".", ".", ".", ".", ".", "."}, {"4", ".", ".", ".", ".", ".", ".", ".", "."}, {"5", ".", ".", ".", ".", ".", ".", ".", "."}, {"6", ".", ".", ".", ".", ".", ".", ".", "."}, {"7", ".", ".", ".", ".", ".", ".", ".", "."}, {"8", ".", ".", ".", ".", ".", ".", ".", "."}, {"9", ".", ".", ".", ".", ".", ".", ".", "."}};
        ValidSudoku validSudoku = new ValidSudoku();
//        System.out.println(validSudoku.isValidSudoku(board));
    }

    private boolean checkIfAlreadyExists (int val, int[] checkerArray, int indx) {
        int checkerVal = checkerArray[indx];
        if((1 << val & checkerVal) == 0) {
            return false;       //does not exist
        }
        return true;
    }

    private void fillIntoChecker (int val, int[] checkerArray, int indx) {
        int temp = 1 << val;
        checkerArray[indx] |= temp;
    }

    public boolean isValidSudoku(char[][] board) {
        int[] row = new int[9];
        int[] col = new int[9];
        int[] cube = new int[9];
        for(int i = 0; i<9; i++) {
            row[i] = 0;
            col[i] = 0;
            cube[i] = 0;
        }
        for(int i = 0; i<9; i++) {
            for(int j = 0; j<9; j++) {

                //Decide row no, col no and cube no
                int rowNo = i, colNo = j, cubeNo = -1;
                //For cube no
                if(i >= 0 && i <=2) {
                    if(j >= 0 && j <= 2) {
                        cubeNo = 0;
                    }
                    if(j >= 3 && j <=5) {
                        cubeNo = 1;
                    }
                    if(j >= 6) {
                        cubeNo = 2;
                    }
                }
                if(i >= 3 && i <=5) {
                    if(j >= 0 && j <= 2) {
                        cubeNo = 3;
                    }
                    if(j >= 3 && j <=5) {
                        cubeNo = 4;
                    }
                    if(j >= 6) {
                        cubeNo = 5;
                    }
                }
                if(i >= 6) {
                    if(j >= 0 && j <= 2) {
                        cubeNo = 6;
                    }
                    if(j >= 3 && j <=5) {
                        cubeNo = 7;
                    }
                    if(j >= 6) {
                        cubeNo = 8;
                    }
                }

                if(board[i][j] != '.') {
                    int val = board[i][j];
                    if(checkIfAlreadyExists(val, row, rowNo)) {
                        return false;
                    }
                    else {
                        fillIntoChecker(val, row, rowNo);
                    }
                    if(checkIfAlreadyExists(val, col, colNo)) {
                        return false;
                    }
                    else {
                        fillIntoChecker(val, col, colNo);
                    }
                    if(checkIfAlreadyExists(val, cube, cubeNo)) {
                        return false;
                    }
                    else {
                        fillIntoChecker(val, cube, cubeNo);
                    }
                }

            }
        }
        return true;
    }
}
