package ArraysAndStrings;

//https://leetcode.com/problems/island-perimeter/description/
public class IslandPerimeter {
    public static void main(String[] args) {
        IslandPerimeter islandPerimeter = new IslandPerimeter();
        int[][] arr =  {{1,1,1,0}};
        System.out.println(islandPerimeter.islandPerimeter(arr));
    }

    public int islandPerimeter(int[][] grid) {
        if(grid == null) {
            return 0;
        }
        int perim = 0;
        for(int i = 0; i < grid.length; i++) {
            for(int j = 0; j < grid[i].length; j++) {
                if(grid[i][j] == 1) {
                    perim ++;
                    while (j < grid[i].length && grid[i][j] == 1) {
                        j++;
                    }
                    perim++;
                }
            }
        }
//        System.out.println("perim: " + perim);
        for(int j = 0; j < grid[0].length; j++) {
            for(int i = 0; i < grid.length; i++) {
                if(grid[i][j] == 1) {
                    perim ++;
                    while (i < grid.length && grid[i][j] == 1) {
                        i++;
                    }
                    perim++;
                }
            }
        }
        return perim;
    }
}
