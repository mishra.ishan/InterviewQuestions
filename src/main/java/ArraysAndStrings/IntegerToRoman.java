package ArraysAndStrings;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class IntegerToRoman {
    Map<Integer, String> romanNumbers;
    IntegerToRoman() {
        romanNumbers = new HashMap<>();
        romanNumbers.put(1, "I");
        romanNumbers.put(2, "II");
        romanNumbers.put(3, "III");
        romanNumbers.put(4, "IV");
        romanNumbers.put(5, "V");
        romanNumbers.put(6, "VI");
        romanNumbers.put(7, "VII");
        romanNumbers.put(8, "VIII");
        romanNumbers.put(9, "IX");
        romanNumbers.put(10, "X");
        romanNumbers.put(20, "XX");
        romanNumbers.put(30, "XXX");
        romanNumbers.put(40, "XL");
        romanNumbers.put(50, "L");
        romanNumbers.put(60, "LX");
        romanNumbers.put(70, "LXX");
        romanNumbers.put(80, "LXXX");
        romanNumbers.put(90, "XC");
        romanNumbers.put(100, "C");
        romanNumbers.put(200, "CC");
        romanNumbers.put(300, "CCC");
        romanNumbers.put(400, "CD");
        romanNumbers.put(500, "D");
        romanNumbers.put(600, "DC");
        romanNumbers.put(700, "DCC");
        romanNumbers.put(800, "DCCC");
        romanNumbers.put(900, "CM");
        romanNumbers.put(1000, "M");
        romanNumbers.put(2000, "MM");
        romanNumbers.put(3000, "MMM");
    }

    public static void main(String[] args) {
        IntegerToRoman itr = new IntegerToRoman();
        System.out.println(itr.intToRoman(3000));
    }

    public String intToRoman(int num) {
        String numStr = String.valueOf(num);
        int tenValue = numStr.length();
        tenValue--;
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i<numStr.length(); i++) {
            int key = (int)Math.pow(10,tenValue--) * (numStr.charAt(i) - '0');
            if(romanNumbers.containsKey(key)) {
                sb.append(romanNumbers.get(key));
            }
        }
        return sb.toString();
    }
}
