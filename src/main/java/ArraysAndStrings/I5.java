package ArraysAndStrings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by ishan.mishra on 8/26/17.
 */
public class I5 {
    public static void main(String[] args) throws IOException {
        String s1;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();
        int spaceCount = 0;
        for(int i=0; i<s.length(); i++) {
            if(s.charAt(i) == ' ') {
                spaceCount++;
            }
        }
        char[] charArray = new char[s.length() + spaceCount*2];
        for(int i=0, j=0; i<charArray.length; ) {
            if(s.charAt(j) == ' ') {
                charArray[i++]='%';
                charArray[i++]='2';
                charArray[i++]='0';
                j++;
            }
            else {
                charArray[i++] = s.charAt(j++);
            }
        }
        System.out.println(charArray);
    }
}
