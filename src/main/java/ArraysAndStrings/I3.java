package ArraysAndStrings;

/**
 * Created by ishan.mishra on 8/22/17.
 */
public class I3 {
    public static void main(String... args) {
        String s = "aaava";
        char[] c = s.toCharArray();
        for(int i=0; i<c.length; i++) {
            System.out.print(c[i]);
        }
        System.out.println();
        int n = c.length;
        int checker = 0;
        for(int i = 0; i<n; i++) {
            int val = c[i] - 'a';
            if((checker & (1 << val)) > 0) {
                for(int j=i; j < n-1; j++) {
                    c[j] = c[j+1];
                }
                --i;
                --n;
                c[n] = '\0';
            }
            checker |= (1 << val);
        }
        System.out.println(c);
    }
}
