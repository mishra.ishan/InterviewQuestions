package ArraysAndStrings;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by ishan.mishra on 8/26/17.
 */
public class I4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str1 = scanner.next();
        String str2 = scanner.next();
        Boolean b1 = isAnagramOpt(str1, str2);
        Boolean b2 = isAnagram(str1, str2);
        System.out.print("Are Strings anagram: " + b2);
    }

    private static Boolean isAnagramOpt(String str1, String str2) {
        return false;
    }

    private static Boolean isAnagram(String str1, String str2) {
        Integer n1 = str1.length(), n2 = str2.length();
        if(!n1.equals(n2)) {
            return false;
        }
        Map<Character, Integer> map = new HashMap<>();
        for(int i=0; i<n1; i++) {
            Character c = str1.charAt(i);
            if(map.containsKey(c)) {
                Integer i1 = map.get(c);
                map.replace(c, i1, ++i1);
            }
            else {
                map.put(c, 0);
            }
        }
        for(int i=0; i<n2; i++) {
            Character c = str2.charAt(i);
            if(!map.containsKey(c)) {
                return false;
            }
            Integer integer = map.get(c);
            if(integer-1 == 0) {
                map.remove(c);
            }
            map.replace(c, integer+1, integer);
        }
        return true;
    }
}
