package ArraysAndStrings;

//https://leetcode.com/problems/number-of-segments-in-a-string/description/
public class NoOfSegmentsInAString {
    public static void main (String[] args) {
        NoOfSegmentsInAString noOfSegmentsInAString = new NoOfSegmentsInAString();
        String s = "";
        System.out.println(noOfSegmentsInAString.countSegments(s));
    }

    public int countSegments(String s) {
        if(s == null || s.length() == 0) {
            return 0;
        }
        Long count = 0l;
        int i = 0;
        while(i < s.length()) {
            if(s.charAt(i) != ' ') {
                while(i < s.length() && s.charAt(i) != ' ') {
                    i++;
                }
                count++;
            }
            else {
                i++;
            }
        }
        if(count > Integer.MAX_VALUE) {
            return Integer.MAX_VALUE;
        }
        return count.intValue();
    }
}
