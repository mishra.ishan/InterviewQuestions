package ArraysAndStrings;

/**
 * Created by ishan.mishra on 8/22/17.
 */
public class I2 {
    public static void main(String[] args) {
        char[] chars = new char[]{'a','b','b','c','d','\0'};
        int start = 0;
        int end = chars.length - 2;
        while(start < end) {
            char temp = chars[start];
            chars[start] = chars [end];
            chars[end] = temp;
            ++start;
            --end;
//            System.out.println(chars);
        }
        System.out.println(chars);
    }
}
