package StacksAndQueues;

import LinkedList.ListNode;

public class StackImpl {
    private ListNode top;
    private ListNode minStackTop;

    public void push(int val) {
        if(top == null) {
            top = new ListNode(val);
            minStackTop = new ListNode(val);
            return;
        }
        if(minStackTop.getVal() >= val) {
            minStackTop.appendAtBeginning(val);
        }
        top.appendAtBeginning(val);
    }

    public int peek() {
        if(top == null) {
            System.out.println("Stack is Empty");
            return Integer.MIN_VALUE;
        }
        return top.getVal();
    }

    public int pop() {
        if(top == null) {
            System.out.println("Stack is Empty");
            return Integer.MIN_VALUE;
        }
        ListNode node = top;
        top = top.getNext();
        node.setNext(null);
        if(node.getVal() == minStackTop.getVal()) {
            ListNode minStackNode = minStackTop;
            minStackTop = minStackTop.getNext();
            minStackNode.setNext(null);
        }
        return node.getVal();
    }

    public int getMin() {
        if(minStackTop == null) {
            System.out.println("Stack is Empty");
            return Integer.MAX_VALUE;
        }
        return minStackTop.getVal();
    }

    public boolean isEmpty() {
        return top==null;
    }

    @Override
    public String toString() {
        return top != null ? top.toString() : "Empty";
    }
}
