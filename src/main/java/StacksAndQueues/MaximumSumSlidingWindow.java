package StacksAndQueues;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

public class MaximumSumSlidingWindow {
    public static void main(String[] args) {
        int[] arr = new int[]{1,3,-1,-3,5,3,6,7};
        int k = 2;
        System.out.println("Original Array: "+ Arrays.toString(arr));
        MaximumSumSlidingWindow maximumSumSlidingWindow = new MaximumSumSlidingWindow();
        int[] windowArray = maximumSumSlidingWindow.maxSumSlidingWindow(arr, k);
        System.out.println("For window size "+k+", output array: "+Arrays.toString(windowArray));
    }

    public int[] maxSumSlidingWindow(int[] arr, int windowSize) {
        int n = arr.length;
        int[] ans = new int[n-(windowSize-1)];
        Deque<Integer> deque = new ArrayDeque<>(windowSize);
        for(int i=0; i<n; i++) {
            int val = arr[i];
            if(!deque.isEmpty()) {
                int frontIndex = deque.peekFirst();
                if (i - frontIndex == windowSize) {
                    deque.pollFirst();
                }
                while (!deque.isEmpty() && arr[deque.peekLast()] <= val) {
                    deque.pollLast();
                }
            }
            deque.addLast(i);
            if(i >= windowSize-1) {
                ans[i - (windowSize-1)] = arr[deque.peekFirst()];
            }
        }
        return ans;
    }
}
