package StacksAndQueues;

public class LargestAreaUnderHistogram {
    public static void main(String[] args) {
        int[] arr = new int[]{3,5,4,6,3,2};
        LargestAreaUnderHistogram largestAreaUnderHistogram = new LargestAreaUnderHistogram();
        System.out.println(largestAreaUnderHistogram.largestAreaUnderHistogram(arr));
    }

    public int largestAreaUnderHistogram(int[] arr) {
        StackImpl stack = new StackImpl();
        int maxArea = Integer.MIN_VALUE;
        int n = arr.length;
        int i =0;
        for(i=0; i<n; i++) {
            if(stack.isEmpty() || arr[stack.peek()] < arr[i]) {
                stack.push(i);
            }
            else {
                while(!stack.isEmpty() && arr[stack.peek()] > arr[i]) {
                    int poppedIndex = stack.pop();
                    int areaTillNow = 0;
                    if(stack.isEmpty()) {
                        areaTillNow = arr[poppedIndex] * (i);
                    }
                    else {
                        areaTillNow = arr[poppedIndex] * (i-stack.peek()-1);
                    }
                    if(areaTillNow > maxArea) {
                        maxArea = areaTillNow;
                    }
                }
                stack.push(i);
            }
        }
        while(!stack.isEmpty()) {
            int poppedIndex = stack.pop();
            int areaTillNow = arr[poppedIndex] * (i-stack.peek()-1);
            if(areaTillNow > maxArea) {
                maxArea = areaTillNow;
            }
        }
        return maxArea;
    }
}
