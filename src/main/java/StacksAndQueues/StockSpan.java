package StacksAndQueues;

import java.util.Arrays;

public class StockSpan {
    public static void main(String[] args) {
        int[] price = new int[]{100,80,60,70,60,75,85};
        System.out.println("Original Array: "+Arrays.toString(price));
        StockSpan stockSpan = new StockSpan();
        System.out.println("Stock Span Array: "+Arrays.toString(stockSpan.stockSpan(price)));
    }

    public int[] stockSpan(int[] arr) {
        int n = arr.length;
        int[] spanArray = new int[n];
        spanArray[0] = 1;
        StackImpl stack = new StackImpl();
        stack.push(0);
        for(int i=1; i<n; i++) {
            while(!stack.isEmpty() && arr[stack.peek()] <= arr[i]) {
                stack.pop();
            }
            spanArray[i] = stack.isEmpty()? (i + 1) : (i - stack.peek());
            stack.push(i);
        }
        return spanArray;
    }
}
