package StacksAndQueues;

import LinkedList.ListNode;

public class QueueImpl {
    ListNode front, rear;

    public QueueImpl() {
        this.front = null;
        this.rear = null;
    }

    public void enqueue(int val) {
        if(this.rear == null) {
            this.front = new ListNode(val);
            this.rear = front;
        }
        else {
            rear.setNext(new ListNode(val));
        }
    }

    public int peek() {
        if(front == null) {
            System.out.println("Queue is Empty");
            return Integer.MIN_VALUE;
        }
        return front.getVal();
    }

    public int dequeue() {
        if(front == null) {
            System.out.println("Queue is Empty");
            return Integer.MIN_VALUE;
        }
        ListNode temp = front;
        front = front.getNext();
        temp.setNext(null);
        return temp.getVal();
    }
}
