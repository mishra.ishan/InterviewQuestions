package StacksAndQueues;

import java.util.Stack;

//https://leetcode.com/problems/valid-parentheses/description/
public class ValidParenthesis {
    public static void main(String[] args) {
        ValidParenthesis validParenthesis = new ValidParenthesis();
        String s = "{";
        System.out.println(validParenthesis.isValid(s));
    }

    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        if(s == null) {
            return true;
        }
        for(int i = 0; i<s.length(); i++) {
            char c = s.charAt(i);
            if(c == '(' || c == '{' || c == '[') {
                stack.add(c);
            }
            else {
                if(stack.isEmpty()) {
                    return false;
                }
                char stackTop = stack.pop();
                switch (c) {
                    case ')' : if(stackTop != '(')
                                    return false;
                                break;
                    case '}' : if(stackTop != '{')
                                    return false;
                                break;
                    case ']' : if(stackTop != '[')
                                    return false;
                                break;
                    default : return false;
                }
            }
        }
        if(stack.isEmpty())
            return true;
        return false;
    }
}
