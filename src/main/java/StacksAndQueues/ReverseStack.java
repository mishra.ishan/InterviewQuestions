package StacksAndQueues;

public class ReverseStack {
    public static void main(String[] args) {
        StackImpl stack = new StackImpl();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        System.out.println("Original stack: "+stack);
        ReverseStack reverseStack = new ReverseStack();
        reverseStack.reverseStack(stack);
        System.out.println("After Reverse: "+stack);
    }

    public void reverseStack(StackImpl stack) {
        if(stack.isEmpty()) {
            return;
        }
        int data = stack.pop();
        reverseStack(stack);
        insertAtBottom(stack, data);
    }

    private void insertAtBottom(StackImpl stack, int data) {
        if(stack.isEmpty()) {
            stack.push(data);
            return;
        }
        int temp = stack.pop();
        insertAtBottom(stack, data);
        stack.push(temp);
    }
}