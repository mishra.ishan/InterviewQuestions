package StacksAndQueues;

import java.util.Stack;

public class TowerOfHanoi {
    Stack<Integer>[] towerNo;
    TowerOfHanoi() {
        towerNo = new Stack[3];
        for(int i = 0; i<3; i++) {
            towerNo[i] = new Stack<Integer>();
        }
    }

    private void initSourceTower(int n) {
        for(int i = n; i > 0 ; i--) {
            towerNo[0].add(i);
        }
    }

    private void printTowers() {
        for(int i = 0; i<3; i++) {
            System.out.println(towerNo[i]);
        }
    }

    public static void main (String[] args) {
        int n = 3;
        TowerOfHanoi towerOfHanoi = new TowerOfHanoi();
        towerOfHanoi.initSourceTower(n);
//        System.out.println("INITIAL:-");
//        towerOfHanoi.printTowers();
        towerOfHanoi.solveFor3Rec(n, 0, 1, 2);
//        System.out.println("FINAL:-");
//        towerOfHanoi.printTowers();
    }


    private void solveFor3Rec (int n, int src, int buffer, int dest) {
        if(n > 0) {
            solveFor3Rec(n - 1, src, dest, buffer);
            moveTop(src, dest);
            solveFor3Rec(n-1, buffer, src, dest);
        }
    }

    private void moveTop (int src, int dest) {
        System.out.println("Moving disk " + towerNo[src].peek() +" from " + src + " to " + dest);
        towerNo[dest].add(towerNo[src].pop());
    }

}
