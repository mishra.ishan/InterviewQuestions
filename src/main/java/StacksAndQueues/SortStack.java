package StacksAndQueues;

public class SortStack {
    public static void main(String[] args) {
        StackImpl stack = new StackImpl();
        stack.push(5);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(1);
        SortStack sortStack = new SortStack();
        System.out.println("Stack in the beginning: "+stack);
        sortStack.sortStack(stack);
        System.out.println("After Sorting: "+stack);
    }

    public void sortStack(StackImpl stack) {
        StackImpl temp = new StackImpl();
        while(!stack.isEmpty()) {
            temp.push(stack.pop());
        }
        while(!temp.isEmpty()) {
            int minVal = temp.pop();
            while (!stack.isEmpty() && minVal <= stack.peek()) {
                int data = stack.pop();
                temp.push(data);
            }
            stack.push(minVal);
        }
    }
}
