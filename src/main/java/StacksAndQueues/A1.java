package StacksAndQueues;


public class A1 {
    public static void main(String[] args) {
        StackImpl stackGetMin = new StackImpl();
        stackGetMin.push(5);
        stackGetMin.push(4);
        stackGetMin.push(3);
        stackGetMin.push(7);
        stackGetMin.push(9);
        stackGetMin.push(3);
        stackGetMin.push(2);
        stackGetMin.push(7);
        System.out.println("Top Element: "+stackGetMin.peek());
        System.out.println("Min Element: "+stackGetMin.getMin());
        System.out.println("Popping: "+stackGetMin.pop()+" GetMin: "+stackGetMin.getMin());
        System.out.println("Popping: "+stackGetMin.pop()+" GetMin: "+stackGetMin.getMin());
        System.out.println("Popping: "+stackGetMin.pop()+" GetMin: "+stackGetMin.getMin());
        System.out.println("Popping: "+stackGetMin.pop()+" GetMin: "+stackGetMin.getMin());
        System.out.println("Popping: "+stackGetMin.pop()+" GetMin: "+stackGetMin.getMin());
        System.out.println("Popping: "+stackGetMin.pop()+" GetMin: "+stackGetMin.getMin());
        System.out.println("Popping: "+stackGetMin.pop()+" GetMin: "+stackGetMin.getMin());
        System.out.println("Popping: "+stackGetMin.pop()+" GetMin: "+stackGetMin.getMin());
    }
}
